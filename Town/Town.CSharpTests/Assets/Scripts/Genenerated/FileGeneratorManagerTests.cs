﻿
#define TISU_TESTS
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    internal class EqualityTestStruct<T> : IEqualityComparer<T>
    {
        readonly Func<T, string> m_getter;
        internal EqualityTestStruct(Func<T, string> a_getter)
        {
            m_getter = a_getter;
        }
        public bool Equals(T x, T y)
        {
            return m_getter(x) == m_getter(y);
        }

        public int GetHashCode(T obj)
        {
            return base.GetHashCode();
        }
    }
    internal class FileGeneratorManagerTestsImpl : FileGeneratorManager
    {
        public FileGeneratorManagerTestsImpl(string a_file_name, eContentType a_content_type, bool a_append = false, string a_custom_content = null)
        : base(a_file_name, a_content_type, a_append, a_custom_content)
        {

        }

        public IEnumerable<T> Distinct<T>(IEnumerable<T> a_container, Func<T, string> a_getter)
        {
            return CustomDistinct(a_container,a_getter);
        }
    }
    struct TestStruct
    {
        public string m_name;
        public TestStruct(string a_name)
        {
            m_name = a_name;
        }
    }
    [TestClass()]
    public class FileGeneratorManagerTests
    {

        const string FILE_DUMMY_NAME = "eDummy";
        const string FOLDER_NAME = @"\Assets\Scripts\Genenerated\AutomaticallyGenerated\";
        static readonly string FILE_PATH = Directory.GetCurrentDirectory() + FOLDER_NAME + FILE_DUMMY_NAME;
        static readonly string FILE_PATH_WITH_EXTENSION = FILE_PATH + ".cs";
        [TestMethod()]
        public void MakeFileTest()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);
            List<TestStruct> input_list = new List<TestStruct>();
            char test_name = 'a';
            for (int i = 0; i < 100; i++)
            {
                input_list.Add(new TestStruct(test_name.ToString()));
                if (i == 20)
                {
                    test_name++;
                }
                if (i == 50)
                {
                    test_name++;
                }
                if (i > 80)
                {
                    test_name++;
                }
            }
            var expected_list = new List<string>();
            test_name = 'a';
            for (int i = 0; i < 21; i++)
            {
                expected_list.Add(test_name.ToString());
                test_name++;
            }

            var method_return = test_object.Distinct(input_list, x => x.m_name);

            Assert.IsTrue(method_return.Count() == expected_list.Count);
            if (method_return.Count() != expected_list.Count)
            {
                return;
            }
            for (int i = 0; i < expected_list.Count; i++)
            {
                Assert.IsTrue(expected_list[i] == method_return.ElementAt(i).m_name);
            }
            Assert.IsTrue(true);
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void OneName()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);

            List<TestStruct> input_list = new List<TestStruct>();
            char test_name = 'a';
            for (int i = 0; i < 100; i++)
            {
                input_list.Add(new TestStruct(test_name.ToString()));
            }

            var method_return = test_object.Distinct(input_list, x => x.m_name);
            Assert.IsTrue(method_return.Count() == 1);
            Assert.IsTrue(method_return.First().m_name == "a");
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void Intersect()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);
            List<TestStruct> input_list = new List<TestStruct>();
            char test_name_1 = 'a';
            char test_name_2 = 'x';
            for (int i = 0; i < 100; i++)
            {
                if (i % 2 == 0)
                {
                    input_list.Add(new TestStruct(test_name_1.ToString()));
                }
                else
                {
                    input_list.Add(new TestStruct(test_name_2.ToString()));
                }
            }

            var method_return = test_object.Distinct(input_list, x => x.m_name);
            Assert.IsTrue(method_return.Count() == 2);
            Assert.IsTrue(method_return.First().m_name == "a");
            Assert.IsTrue(method_return.Last().m_name == "x");
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void AllDistinct()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);
            List<TestStruct> input_list = new List<TestStruct>();
            char test_name = ' ';
            int sign_count = '~' - ' ';
            for (int i = 0; i < sign_count; i++)
            {
                input_list.Add(new TestStruct(test_name.ToString()));
                test_name++;
            }

            var method_return = test_object.Distinct(input_list, x => x.m_name);

            Assert.IsTrue(method_return.Count() == sign_count);
            char expected_name = ' ';
            foreach (var item in method_return)
            {
                Assert.IsTrue(item.m_name == expected_name.ToString());
                expected_name++;
            }
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void AllTwoTimes()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);
            List<TestStruct> input_list = new List<TestStruct>();
            char test_name = ' ';
            int sign_count = '~' - ' ';
            for (int i = 0; i < sign_count * 2; i++)
            {
                input_list.Add(new TestStruct(test_name.ToString()));
                if (i % 2 == 0 && i != 0)
                {
                    test_name++;
                }
            }

            var method_return = test_object.Distinct(input_list, x => x.m_name);

            Assert.IsTrue(method_return.Count() == sign_count);
            char expected_name = ' ';
            foreach (var item in method_return)
            {
                Assert.IsTrue(item.m_name == expected_name.ToString());
                expected_name++;
            }
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void OneRecord()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);

            List<TestStruct> input_list = new List<TestStruct>();
            string test_name = "test_very_long_but_single";
            input_list.Add(new TestStruct(test_name));
            var method_return = test_object.Distinct(input_list, x => x.m_name);
            Assert.IsTrue(method_return.Count() == 1);
            Assert.IsTrue(method_return.First().m_name == test_name);
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void TwoRecords()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);

            List<TestStruct> input_list = new List<TestStruct>();
            string test_1 = "test";
            string test_2 = "test2";
            input_list.Add(new TestStruct(test_1));
            input_list.Add(new TestStruct(test_2));
            var method_return = test_object.Distinct(input_list, x => x.m_name);
            Assert.IsTrue(method_return.Count() == 2);
            Assert.IsTrue(method_return.First().m_name == test_1);
            Assert.IsTrue(method_return.Last().m_name == test_2);
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void InMiddleRepeat()
        {
            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);

            List<TestStruct> input_list = new List<TestStruct>();
            string test_1 = "test";
            string in_the_middle = "middle";
            input_list.Add(new TestStruct(test_1));
            for (int i = 0; i < 100; i++)
            {
                input_list.Add(new TestStruct(in_the_middle));
            }
            string test_2 = "test2";
            input_list.Add(new TestStruct(test_2));
            var method_return = test_object.Distinct(input_list, x => x.m_name);
            Assert.IsTrue(method_return.Count() == 3);
            Assert.IsTrue(method_return.First().m_name == test_1);
            Assert.IsTrue(method_return.ElementAt(1).m_name == in_the_middle);
            Assert.IsTrue(method_return.Last().m_name == test_2);
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void Random()
        {
            char last_sign = '~';
            char first_sign = ' ';
            List<TestStruct> input_list = new List<TestStruct>();
            var random_generator = new System.Random();

            for (int i = 0; i < 1000; i++)
            {
                input_list.Add(new TestStruct(((char)random_generator.Next(first_sign, last_sign + 1)).ToString()));
            }

            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);
            var method_result = test_object.Distinct(input_list, x => x.m_name);
            var linq_result = input_list.Distinct(new EqualityTestStruct<TestStruct>(x => x.m_name));

            Assert.IsTrue(method_result.Count() == linq_result.Count());
            test_object.Dispose();
            DeleteDummyFile();
        }
        [TestMethod()]
        public void Performance()
        {
            char last_sign = '~';
            char first_sign = ' ';
            List<TestStruct> input_list = new List<TestStruct>();
            var random_generator = new System.Random();

            for (int i = 0; i < 10000000; i++)
            {
                input_list.Add(new TestStruct(((char)random_generator.Next(first_sign, last_sign + 1)).ToString()));
            }

            var test_object = new FileGeneratorManagerTestsImpl(FILE_DUMMY_NAME, eContentType.type_enum);

            var timer_linq = Stopwatch.StartNew();
            var linq_result = input_list.Distinct(new EqualityTestStruct<TestStruct>(x => x.m_name)).ToList();
            timer_linq.Stop();
            var linq_timer_result = timer_linq.Elapsed;

            var timer = Stopwatch.StartNew();
            var method_result = test_object.Distinct(input_list, x => x.m_name);
            timer.Stop();
            var method_timer_result = timer.Elapsed;          
           
           
            Assert.IsTrue(( linq_timer_result) >= ( method_timer_result));
            test_object.Dispose();
            DeleteDummyFile();
        }
        private void DeleteDummyFile()
        {
            if (File.Exists(FILE_PATH_WITH_EXTENSION))
            {
                File.Delete(FILE_PATH_WITH_EXTENSION);
            }
        }
    }
}
