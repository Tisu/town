﻿
using UnityEngine;

public struct KeyMapper
{
    public const KeyCode CAMERA_MOVE_LEFT = KeyCode.A;
    public const KeyCode CAMERA_MOVE_RIGHT = KeyCode.D;
    public const KeyCode CAMERA_MOVE_UP = KeyCode.W;
    public const KeyCode CAMERA_MOVE_DOWN = KeyCode.S;
    public const KeyCode CAMERA_ZOOM_IN = KeyCode.E;
    public const KeyCode CAMERA_ZOOM_OUT = KeyCode.Q;
}
