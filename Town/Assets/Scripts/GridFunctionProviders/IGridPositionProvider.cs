﻿using Assets.Scripts.HexGrid.Hex;
using UnityEngine;

public interface IGridPositionProvider
{
    Vector3 GetGridPosition(Vector3 worldPosition);
    Vector3 GridToWorld(Vector3 gridPosition);

    Vector3 GetWorldPositionOfCellCenter(Vector3 gridPosition);

    void AlignTransformToGrid(Transform transform);
    Vector3 AlignWorldPositionToGrid(Vector3 position);
    int GridDistanceBetweenGrids(GridCellInfo first, GridCellInfo second);
    int GridDistanceBetweenGrids(HexGrid first, HexGrid second);
    Vector4 WorldToCubic(Vector3 world_position);
    Vector4 GridToCubic(Vector3 grid_position);
    Vector3 WorldToGrid(Vector3 world);
    Vector3 CubicToGrid(Vector4 cubic_position);
    Vector3 CubicToWorld(Vector3 cubic_position);
    Vector3 CubicToWorld(HexGrid cubic_position);
}
