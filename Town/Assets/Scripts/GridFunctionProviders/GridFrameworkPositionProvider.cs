﻿using UnityEngine;
using Assets.Scripts.HexGrid.Hex;

public class GridFrameworkPositionProvider : MonoBehaviour, IGridPositionProvider
{
    [SerializeField]
    private GFHexGrid hexGrid;
    private static GridFrameworkPositionProvider instance;
    public static IGridPositionProvider m_instance
    {
        get
        {
            if(!instance)
            {
                instance = GameObject.Find("GridFrameworkProvider").GetComponent<GridFrameworkPositionProvider>();
            }
            return instance;
        }
    }

    void Awake()
    {
        if(!instance)
        {
            instance = this;
        }
    }
    public void AlignTransformToGrid(Transform transform)
    {
        hexGrid.AlignTransform(transform);
    }

    public Vector3 AlignWorldPositionToGrid(Vector3 position)
    {
        return hexGrid.AlignVector3(position);
    }
    public Vector3 GetGridPosition(Vector3 worldPosition)
    {
        return hexGrid.WorldToGrid(worldPosition);
    }
    public Vector3 GridToWorld(Vector3 gridPosition)
    {
        return hexGrid.GridToWorld(gridPosition);
    }
    public Vector3 GetWorldPositionOfCellCenter(Vector3 gridPosition)
    {
        return hexGrid.NearestBoxW(GridToWorld(gridPosition));
    }
    public int GridDistanceBetweenGrids(GridCellInfo first, GridCellInfo second)
    {
        return GridDistanceBetweenGrids(first.m_hex_grid, second.m_hex_grid);
    }
    public int GridDistanceBetweenGrids(HexGrid first, HexGrid second)
    {
        var x = Mathf.Abs(first.m_x - second.m_x);
        var y = Mathf.Abs(first.m_y - second.m_y);
        var z = Mathf.Abs(first.m_z - second.m_z);
        return Mathf.Max(x, y, z);
    }
    public Vector4 WorldToCubic(Vector3 world_position)
    {
        return hexGrid.WorldToCubic(world_position);
    }
    public Vector3 CubicToWorld(Vector3 cubic_position)
    {
        return hexGrid.CubicToWorld(cubic_position);
    }
    public Vector3 CubicToWorld(HexGrid cubic_position)
    {
        return hexGrid.CubicToWorld(new Vector4(cubic_position.m_x, cubic_position.m_y, cubic_position.m_z, 0));
    }
    public Vector4 GridToCubic(Vector3 grid_position)
    {
        return hexGrid.HerringUToCubic(grid_position);
    }
    public Vector3 WorldToGrid(Vector3 world)
    {
        return  hexGrid.WorldToHerringU(world);
    }
    public Vector3 CubicToGrid(Vector4 cubic_position)
    {
        return hexGrid.CubicToHerringU(cubic_position);
    }
}
