﻿namespace Commands
{
    public sealed class Command
    {
        public CommandType m_type { get; private set; }
        public CommandExecutionType m_execution_type { get; private set; }
        public Command(CommandType type, CommandExecutionType execution_type)
        {
            m_type = type;
            m_execution_type = execution_type;
        }

        public bool CanBeInterrupted()
        {
            return m_execution_type == CommandExecutionType.PeriodicCanBeInterrupted;
        }

        public bool IsPeriodic()
        {
            return m_execution_type == CommandExecutionType.PeriodicCanBeInterrupted ||
                   m_execution_type == CommandExecutionType.Periodic;
        }

        public bool IsMoreImportant(Command other)
        {
            return m_type > other.m_type || CanBeInterrupted();
        }
    }
}