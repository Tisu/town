﻿namespace Commands
{
    public enum CommandExecutionType
    {
        Immediately,
        PeriodicCanBeInterrupted,
        Periodic
    }
}