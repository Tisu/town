﻿using UnityEngine;

namespace Commands
{
    public interface ICommandManager
    {
        void RunActionClick(Collider2D hit);
        bool InterruptExecution();
        void SubscribeRunner(string tag, IHitRunner hit_runner);
        void RunMousePositionHit(Collider2D hit);
    }
}