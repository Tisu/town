﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Commands
{
    public sealed class CommandManager : INotifyEndOfCommand, ICommandManager
    {
        readonly Dictionary<string, IHitRunner> m_hit_runners_tags = new Dictionary<string, IHitRunner>();
        private IHitRunner m_current_executing_runner;
        public void SubscribeRunner(string tag, IHitRunner hit_runner)
        {
            m_hit_runners_tags.Add(tag, hit_runner);
        }
        public void RunActionClick(Collider2D hit)
        {
            IHitRunner hit_runner;
            if(!m_hit_runners_tags.TryGetValue(hit.gameObject.tag, out hit_runner))
            {
                if(!CheckIfCurrentlyExecutingCommand())
                {
                    return;
                }
                if(m_current_executing_runner.IsPeriodicSubscribeFor(hit.tag))
                {
                    InvokeOnRunner(m_current_executing_runner, hit);
                    return;
                }

                m_current_executing_runner.InterruptExecution();
                return;
            }
            if(m_current_executing_runner == null)
            {
                RunNewCommand(hit, hit_runner);
                return;
            }
            ManageRunningCommand(hit_runner, hit);
        }

        private void InvokeOnRunner(IHitRunner runner, Collider2D hit)
        {
            runner.RunActionClick(hit, this);
        }
        public bool InterruptExecution()
        {
            if(m_current_executing_runner == null)
            {
                return false;
            }
            m_current_executing_runner.InterruptExecution();
            NotifyEndOfCommand();
            return true;
        }

        public void RunMousePositionHit(Collider2D hit)
        {
            if(m_current_executing_runner == null)
            {
                return;
            }
            m_current_executing_runner.RunMousePositionHit(hit);
        }
        private void RunNewCommand(Collider2D hit, IHitRunner hit_runner)
        {
            InvokeOnRunner(hit_runner, hit);
            if(hit_runner.GetCommand().IsPeriodic())
            {
                m_current_executing_runner = hit_runner;
            }
        }
        public void NotifyEndOfCommand()
        {
            m_current_executing_runner = null;
        }
        private bool CheckIfCurrentlyExecutingCommand()
        {
            return m_current_executing_runner != null;
        }
        private void ManageRunningCommand(IHitRunner hit_runner, Collider2D hit)
        {
            if(hit_runner.GetCommand().IsMoreImportant(m_current_executing_runner.GetCommand()))
            {
                m_current_executing_runner.InterruptExecution();
                m_current_executing_runner = null;
                RunNewCommand(hit, hit_runner);
                return;
            }
            InvokeOnRunner(m_current_executing_runner, hit);
        }
    }
}