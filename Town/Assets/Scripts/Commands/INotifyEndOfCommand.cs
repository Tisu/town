﻿namespace Commands
{
    public interface INotifyEndOfCommand
    {
        void NotifyEndOfCommand();
    }
}