﻿static class ResourceChecker
{
    public static bool CheckResources(IBuildingInformation a_building_information_cost_needed)
    {
        if(a_building_information_cost_needed == null)
        {
            return false;
        }
        return ResourcesManager.m_instance.m_stone >= a_building_information_cost_needed.GetStoneCost()
            && ResourcesManager.m_instance.m_wood >= a_building_information_cost_needed.GetWoodCost()
            && ResourcesManager.m_instance.CheckIfEnoughPopulation(a_building_information_cost_needed);
    }

    public static bool CheckIfEnoughtWood(uint wood)
    {
        return ResourcesManager.m_instance.m_wood >= wood;
    }
    public static bool CheckIfEnoughtStone(uint stone)
    {
        return ResourcesManager.m_instance.m_stone >= stone;
    }
    public static bool CheckIfEnoughtResources(uint wood, uint stone)
    {
        return CheckIfEnoughtStone(stone) && CheckIfEnoughtWood(wood);
    }
}

