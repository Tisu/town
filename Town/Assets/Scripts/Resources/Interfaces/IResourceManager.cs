﻿
public interface IResourcesManager
{
    uint m_stone { get; set; }
    uint m_wood { get; set; }
    int m_population { get; set; }
    int m_food { get; set; }
    int m_max_population { get; set; }
    bool SubtractBuildingCost(IBuildingInformation building_information_cost);
    event ResourcesManager.ChangeResourcesCallback m_change_resources_callback;
    bool CheckIfEnoughPopulation(IBuildingInformation building_information_cost);
    bool CheckIfEnoughPopulationForMainDisplay();
}

