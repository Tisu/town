﻿[System.Serializable]
public struct sResource
{
    public uint m_wood_cost;
    public uint m_stone_cost;
    public sResource(uint a_wood,uint a_stone)
    {
        m_wood_cost = a_wood;
        m_stone_cost = a_stone;
    }
}
