﻿using System.Linq;
using Assets.Scripts.Buildings;
using Assets.Scripts.UI_Managers.Resources;
using UnityEngine;

public class ResourcesManager : MonoBehaviour, IResourcesManager
{
    [SerializeField]
    private uint _stone = 10;
    [SerializeField]
    private uint _wood = 20;
    [SerializeField]
    private MainUIResources m_ui_resources_manager;
    [SerializeField]
    private int _food;
    private int _population;

    [SerializeField]
    private int _max_population;

    public delegate void ChangeResourcesCallback();
    public event ChangeResourcesCallback m_change_resources_callback;
    public uint m_stone
    {
        get { return _stone; }
        set
        {
            m_ui_resources_manager.SetStone(value);
            _stone = value;
            InformResourceChange();
        }
    }
    public uint m_wood
    {
        get { return _wood; }
        set
        {
            m_ui_resources_manager.SetWood(value);
            _wood = value;
            InformResourceChange();
        }
    }

    public int m_population
    {
        get
        {
            return _population;
        }
        set
        {
            _population = value;
            m_ui_resources_manager.SetPopulation(value, m_max_population);
        }
    }
    public int m_max_population
    {
        get
        {
            return _max_population;
        }
        set
        {
            _max_population = value;
            m_ui_resources_manager.SetPopulation(m_population, value);
        }
    }
    public int m_food
    {
        get
        {
            return _food;
        }
        set
        {
            _food = value;
            m_ui_resources_manager.SetFood(value);
            InformResourceChange();
        }
    }
    public static IResourcesManager m_instance
    {
        get
        {
            return _instance ?? (_instance = GameObject.Find("ResourcesManager").GetComponent<ResourcesManager>());
        }
        private set
        {
            _instance = value;
        }
    }

    private static IResourcesManager _instance;
    void Awake()
    {
        _instance = this;
        m_stone = m_stone;
        m_wood = m_wood;
        InformResourceChange();
    }

    void OnValidate()
    {
        m_stone = m_stone;
        m_wood = m_wood;
        m_population = m_population;
        m_food = m_food;
    }


    public bool SubtractBuildingCost(IBuildingInformation building_information_cost)
    {
        if(!ResourceChecker.CheckResources(building_information_cost))
        {
            return false;
        }
        if(!CheckIfEnoughPopulation(building_information_cost))
        {
            return false;
        }
        m_stone -= building_information_cost.GetStoneCost();
        m_wood -= building_information_cost.GetWoodCost();
        if(!CheckIfBuildingDoesntNeedPopulation(building_information_cost))
        {
            m_population += BuildingsConfiguration.POPULATION_NEEDED_PER_BUILDING;
        }
        InformResourceChange();
        return true;
    }

    public bool CheckIfEnoughPopulation(IBuildingInformation building_information)
    {
        return CheckIfEnoughPopulationForMainDisplay()
            || CheckIfBuildingDoesntNeedPopulation(building_information);
    }

    private static bool CheckIfBuildingDoesntNeedPopulation(IBuildingInformation building_information)
    {
        return BuildingsConfiguration.BUILDINGS_DOESNT_NEED_POPULATION.Contains(building_information.GetName());
    }

    public bool CheckIfEnoughPopulationForMainDisplay()
    {
        return ((m_max_population - m_population) >= BuildingsConfiguration.POPULATION_NEEDED_PER_BUILDING);
    }

    private void InformResourceChange()
    {
        if(m_change_resources_callback != null)
        {
            m_change_resources_callback();
        }
    }
}

