﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

namespace Assets.Scripts.EventSystem
{
    public sealed class CustomStandaloneInputModule : StandaloneInputModule
    {
        readonly List<RaycastResult> m_result = new List<RaycastResult>();
        public bool IsPointedOverInventory()
        {
            PointerEventData pointer;
            if (!m_PointerData.TryGetValue(kMouseLeftId, out pointer))
            {
                return false;
            }

            if(pointer.pointerEnter == null)
            {
                return false;
            }

            eventSystem.RaycastAll(pointer, m_result);
            return m_result.Any(raycast => raycast.gameObject.tag == Tags.Tags.INVENTORY || raycast.gameObject.tag  == Tags.Tags.CANNOT_CLICK_OVER);
        }
    }
}