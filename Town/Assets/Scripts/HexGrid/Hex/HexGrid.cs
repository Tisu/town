﻿using UnityEngine;

namespace Assets.Scripts.HexGrid.Hex
{
    public struct HexGrid
    {
        public int m_x { get; private set; }
        public int m_y { get; private set; }
        public int m_z { get; private set; }
        public HexGrid(int x, int y, int z)
        {
            m_x = x;
            m_y = y;
            m_z = z;
        }
        public HexGrid(Vector3 cubic)
        {
            m_x = (int)cubic.x;
            m_y = (int)cubic.y;
            m_z = (int)cubic.z;
        }
        public void SetCoordinatesFromWorldPosition(Vector3 world_position)
        {
            var world = GridFrameworkPositionProvider.m_instance.AlignWorldPositionToGrid(world_position);
            var cubic = GridFrameworkPositionProvider.m_instance.WorldToCubic(world);
            m_x = Mathf.RoundToInt(cubic.x);
            m_y = Mathf.RoundToInt(cubic.y);
            m_z = Mathf.RoundToInt(cubic.z);
        }

        public Vector3 ToVector3()
        {
            return new Vector3(m_x, m_y, m_z);
        }
        public Vector3 WorldCoordinates()
        {
            return GridFrameworkPositionProvider.m_instance.CubicToWorld(new Vector3(m_x, m_y, m_z));
        }
        public Vector3 HexCoordinates()
        {
            return GridFrameworkPositionProvider.m_instance.CubicToGrid(new Vector4(m_x, m_y, m_z, 0));
        }
        public bool Equals(HexGrid other)
        {
            return m_x == other.m_x && m_y == other.m_y && m_z == other.m_z;
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj)) return false;
            return obj is HexGrid && Equals((HexGrid)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = m_x;
                hashCode = (hashCode * 397) ^ m_y;
                hashCode = (hashCode * 397) ^ m_z;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "x :" + m_x + " y: " + m_y + "z: " + m_z;
        }

        public static bool operator ==(HexGrid first, HexGrid second)
        {
            return first.m_x == second.m_x && first.m_y == second.m_y && first.m_z == second.m_z;
        }
        public static bool operator !=(HexGrid first, HexGrid second)
        {
            return !(first == second);
        }
    }
}


