﻿struct HexMeasure
{
    public const float OUTER_RADIUS = 0.69f;
    public const float INNER_RADIUS = 0.6f;
    public const float INNER_DIAMETER = 1.2f;
}

