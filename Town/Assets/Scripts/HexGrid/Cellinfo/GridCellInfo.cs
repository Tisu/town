﻿using System.Linq.Expressions;
using Assets.Scripts.HexGrid.Hex;
using Assets.Scripts.ProgressBar;
using Assets.Scripts.SpriteExtender;
using UnityEngine;

public sealed class GridCellInfo : MonoBehaviour
{
    private static readonly Color32 MARKED_GRID_COLOR = new Color32(194, 194, 194, 255);
    public HexGrid m_hex_grid = new HexGrid();
    public SpriteRenderer m_sprite_renderer { get; private set; }
    public ProgressBar m_process_bar { get; set; }

    void Awake()
    {
        m_hex_grid.SetCoordinatesFromWorldPosition(transform.position);
        m_sprite_renderer = GetComponent<SpriteRenderer>();
    }
    public bool IsBusy()
    {
        return m_process_bar != null;
    }
    public void SetSpriteColorNormal()
    {
        m_sprite_renderer.color = Color.white;
    }
    public void SetSpriteColorRangeDisplay()
    {
        m_sprite_renderer.color = MARKED_GRID_COLOR;
    }
    private bool Equals(GridCellInfo other)
    {
        return base.Equals(other) && m_hex_grid.Equals(other.m_hex_grid);
    }
    public bool IsDependent(string dependent)
    {
        return m_sprite_renderer.sprite.IsDependent(dependent);
    }
    public bool IsWood()
    {
        return m_sprite_renderer.sprite.IsWood();
    }
    public bool IsStone()
    {
        return m_sprite_renderer.sprite.IsStone();
    }
    public string GetSpriteName()
    {
        return m_sprite_renderer.sprite.name;
    }
    public override bool Equals(object obj)
    {
        if(ReferenceEquals(null, obj))
            return false;
        if(ReferenceEquals(this, obj))
            return true;
        return obj is GridCellInfo && Equals((GridCellInfo)obj);
    }
    public override int GetHashCode()
    {
        unchecked
        {
            return (base.GetHashCode() * 397) ^ m_hex_grid.GetHashCode();
        }
    }
}
