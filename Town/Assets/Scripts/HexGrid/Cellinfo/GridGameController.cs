﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.HexGrid.Hex;
using UnityEngine;

public sealed class GridGameController : IEnumerable<GridCellInfo>
{
    private readonly Dictionary<HexGrid, GridCellInfo> m_spawned_grid = new Dictionary<HexGrid, GridCellInfo>();
    private static GridGameController m_singletone_instance;
    public static GridGameController m_instance
    {
        get { return m_singletone_instance ?? (m_singletone_instance = new GridGameController()); }
    }

    private GridGameController()
    {
        var terrain_parent = GameObject.Find("Terrain Parent");
        var grid_cell_infos = terrain_parent.GetComponentsInChildren<GridCellInfo>();
        foreach(var grid_cell_info in grid_cell_infos)
        {
            AddSpawnedGrid(grid_cell_info);
        }
    }

    public void AddSpawnedGrid(GridCellInfo grid)
    {
        m_spawned_grid.Add(grid.m_hex_grid, grid);
    }
    public GridCellInfo GetCellInfo(HexGrid hex_grid)
    {
        GridCellInfo grid_cell_info;
        if(m_spawned_grid.TryGetValue(hex_grid, out grid_cell_info))
        {
            return grid_cell_info;
        }
        return null;
    }

    public IEnumerator<GridCellInfo> GetEnumerator()
    {
        foreach (var grid in m_spawned_grid.Values)
        {
            yield return grid;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
