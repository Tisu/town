﻿using Assets.Scripts.AssetsManagement;
using UnityEngine;

public class SpawnGridMono : MonoBehaviour, ICustomEditor
{
    [SerializeField]
    int m_width = 0;
    [SerializeField]
    int m_heigth = 0;
    [SerializeField]
    int m_octaves = 0;
    [SerializeField]
    [Range(0, 1)]
    float m_persistance = 0;
    [SerializeField]
    float m_lacunarity;
    [SerializeField]
    int m_seed = 0;
    [SerializeField]
    Vector2 m_offset;
    [SerializeField]
    float m_scale;


    public bool m_auto_update;
    SpawnGrid m_grid_spawner;

    void OnValidate()
    {
        ValidateMembers();
    }

    private void ValidateMembers()
    {
        if(m_width < 1)
        {
            m_width = 1;
        }
        if(m_heigth < 1)
        {
            m_heigth = 1;
        }
        if(m_lacunarity < 1)
        {
            m_lacunarity = 1;
        }
        if(m_octaves < 0)
        {
            m_octaves = 0;
        }
        if(m_scale <= 0)
        {
            m_scale = 0.0001f;
        }
        if(m_seed <= 0)
        {
            m_seed = 1;
        }
    }

    public void Generate()
    {
        m_grid_spawner = m_grid_spawner ?? new SpawnGrid();

        m_grid_spawner.CreateTerrain(PerlinNoise.GenerateNoise(
            m_width,
            m_heigth,
            m_seed,
            m_scale,
            m_octaves,
            m_persistance,
            m_lacunarity,
            m_offset),
            LoadCache.LoadSprite(), Vector2.zero);
    }
    public void Generate(Vector2 draw_origin)
    {
        m_grid_spawner = m_grid_spawner ?? new SpawnGrid();

        m_grid_spawner.CreateTerrain(PerlinNoise.GenerateNoise(
            m_width,
            m_heigth,
            Random.Range(0, int.MaxValue),
            m_scale,
            m_octaves,
            m_persistance,
            m_lacunarity,
            m_offset),
            LoadCache.LoadSprite(), draw_origin);
    }

    public int GetHeight()
    {
        return m_heigth;
    }

    public int GetWidth()
    {
        return m_width;
    }
}
