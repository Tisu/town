﻿using UnityEngine;

public static class PerlinNoise
{
    public static float[,] GenerateNoise
       (int a_width,
        int a_height,
        int a_seed,
        float a_scale,
        int a_octaves,
        float a_persistance,
        float a_lacunarity,
        Vector2 a_offset)
    {
        float[,] noise_array = new float[a_width + 1, a_height + 1];
        System.Random random_generator = new System.Random(a_seed);
        Vector2[] octaves_offset = new Vector2[a_octaves + 1 ];
        for (int i = 0; i <= a_octaves; i++)
        {
            octaves_offset[i] = new Vector2
                (random_generator.Next(-100000, 100000) + a_offset.x,
                random_generator.Next(-100000, 100000) + a_offset.y);
        }       
        float max_noise = float.MinValue;
        float min_noise = float.MaxValue;
        float half_width = (float)a_width / 2;
        float half_height = (float)a_height / 2;

        float sample_x = 0f;
        var sample_y = 0f;
        var perlin_noise_value = 0f;
        for (int y = 0; y <= a_height; y++)
        {
            for (int x = 0; x <= a_width; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noise_height = 1;
                for (int i = 0; i <= a_octaves; i++)
                {
                    sample_x = (x - half_width) / a_scale * frequency + octaves_offset[i].x;
                    sample_y = (y - half_height) / a_scale * frequency + octaves_offset[i].y;
                    perlin_noise_value = Mathf.PerlinNoise(sample_x, sample_y) * 2 - 1;
                    noise_height += perlin_noise_value * amplitude;                
                    amplitude *= a_persistance;
                    frequency *= a_lacunarity;
                }
                if (noise_height > max_noise)
                {
                    max_noise = noise_height;
                }
                else if (noise_height < min_noise)
                {
                    min_noise = noise_height;
                }
                noise_array[x, y] = noise_height;
            }
        }
        for (int y = 0; y <= a_height; y++)
        {
            for (int x = 0; x <= a_width; x++)
            {
                noise_array[x, y] = Mathf.InverseLerp(min_noise, max_noise, noise_array[x, y]);
            }
        }

        return noise_array;
    }
}
