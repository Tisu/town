﻿using Assets.Scripts.SpriteExtender;
using Assets.Scripts.Tags;
using UnityEngine;

public class SpawnGrid
{
    private const string PARRENT_GAMEOBJECT = "Terrain Parent";
    public void CreateTerrain(float[,] a_noise, SpriteCache a_sprite_to_spawn, Vector2 draw_origin)
    {
        if(a_sprite_to_spawn.m_sprite_cache.Count == 0)
        {
            Tisu.Logger.LogError("Empty terrain definition! ");
            return;
        }
        var parent = GameObject.Find(PARRENT_GAMEOBJECT);

        GameObject terrain_parent = parent == null ? new GameObject { name = PARRENT_GAMEOBJECT } : parent;
        terrain_parent.transform.position = Vector3.zero;
        int half_x = a_noise.GetLength(0) / 2 - (int)draw_origin.x;
        int half_y = a_noise.GetLength(1) / 2 - (int)draw_origin.y;
        for(int y = 0; y < a_noise.GetLength(1); y++)
        {
            for(int x = 0; x < a_noise.GetLength(0); x++)
            {
                GameObject spawn_gameobject = new GameObject();

                SpriteRenderer sprite_renderer = spawn_gameobject.AddComponent<SpriteRenderer>();
                var grid_cell_info = spawn_gameobject.AddComponent<GridCellInfo>();

                var grid = new Vector3(x - half_x, y - half_y, 0);


                sprite_renderer.sprite = ChooseSprite(x, y, ref a_noise, ref a_sprite_to_spawn);
                spawn_gameobject.transform.position =
                    GridFrameworkPositionProvider.m_instance.GetWorldPositionOfCellCenter(grid);
                spawn_gameobject.transform.parent = terrain_parent.transform;
                spawn_gameobject.AddComponent<PolygonCollider2D>();

                spawn_gameobject.tag = sprite_renderer.sprite.IsResource() ? Tags.RESOURCE : Tags.HEXGRID;
                if(IsGeneratedInGame(draw_origin))
                {
                    grid_cell_info.m_hex_grid.SetCoordinatesFromWorldPosition(spawn_gameobject.transform.position);
                    GridGameController.m_instance.AddSpawnedGrid(grid_cell_info);
                }
            }
        }
    }

    private static bool IsGeneratedInGame(Vector2 draw_origin)
    {
        return draw_origin != Vector2.zero;
    }

    private static Sprite ChooseSprite(int x, int y, ref float[,] a_noise, ref SpriteCache a_terrain)
    {
        foreach(var terrain_definition in a_terrain.m_sprite_cache)
        {
            if(a_noise[x, y] <= terrain_definition.m_height)
            {
                Tisu.Logger.LogError(terrain_definition.m_sprite == null, "Sprite is null");
                return terrain_definition.m_sprite;
            }
        }
        Tisu.Logger.LogError("Set one to point 1 to not generate voids ");
        return null;
    }

}
