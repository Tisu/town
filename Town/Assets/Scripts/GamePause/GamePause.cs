﻿namespace Assets.Scripts.GamePause
{
    public sealed class GamePause
    {
        public static bool m_is_game_paused;
        public static float m_pause_duration;
    }
}