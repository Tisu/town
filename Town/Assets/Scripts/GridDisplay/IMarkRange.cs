﻿using Assets.Scripts.HexGrid.Hex;
using UnityEngine;

public interface IMarkRange
{
    void MarkDistance(GameObject game_object, int distance);
    void MarkDistance(HexGrid hex_grid, int distance);
    void RemoveMarkedDistance();
}