﻿using Assets.Scripts.AssetsManagement.Caches.Building;
using Assets.Scripts.SpriteExtender;
using UnityEngine;

namespace Assets.Scripts.GridDisplay
{
    public sealed class MarkBuilding : ISpecialMark
    {
        private static readonly Color32 m_resource_in_range = Color.green;
        private static readonly Color32 m_red_destroy_warning = new Color32(233, 100, 0, 255);
        private IBuildingInformation m_building_information_dependency;
        private HexGrid.Hex.HexGrid? m_start_hex;
        public void SetResourceDependecy(IBuildingInformation building_information, HexGrid.Hex.HexGrid? start_hex)
        {
            m_building_information_dependency = building_information;
            m_start_hex = start_hex;
        }
        public bool MarkSpecialRange(GridCellInfo grid)
        {
            var renderer = grid.GetComponent<SpriteRenderer>();
            if(WillDestroyResource(grid))
            {
                renderer.color = m_red_destroy_warning;
                return true;
            }
            var dependency = m_building_information_dependency.GetResourceDependency();
            if(IsDependendOnTag(grid, dependency))
            {
                renderer.color = m_resource_in_range;
                return true;
            }
            if(IsDependendOnBuildingName(renderer.sprite.name, dependency))
            {
                renderer.color = m_resource_in_range;
                return true;
            }
            if(renderer.sprite.IsDependent(dependency))
            {
                renderer.color = m_resource_in_range;
                return true;
            }
            return false;
        }
        private bool IsDependendOnBuildingName(string name, string dependency)
        {
            return dependency[0] == ObjectType.BUILDING_NAME_DEPENDENCY_MARKER && name == dependency.Substring(1);
        }
        private bool IsDependendOnTag(GridCellInfo grid, string dependency)
        {
            return dependency[0] == ObjectType.TAG_DEPENDENCY_MARKER
                   && grid.tag == dependency.Substring(1)
                   && m_start_hex != null &&
                   grid.m_hex_grid != m_start_hex.Value;
        }
        private bool WillDestroyResource(GridCellInfo grid)
        {
            return m_start_hex.HasValue && m_start_hex == grid.m_hex_grid && IsOccupied(grid);
        }
        private bool IsNotDependent(GridCellInfo grid)
        {
            return !grid.IsDependent(m_building_information_dependency.GetResourceDependency()) && m_building_information_dependency.GetResourceDependency() != ObjectType.UNDEFINED;
        }
        private bool IsOccupied(GridCellInfo grid)
        {
            return (grid.tag == Tags.Tags.RESOURCE && 
                HasActionRange())
                 || (DoesntHaveRange() && IsNotDependent(grid));
        }

        private bool DoesntHaveRange()
        {
            return !HasActionRange();
        }

        private bool HasActionRange()
        {
            return m_building_information_dependency.GetActionRange() != 0;
        }
    }
}