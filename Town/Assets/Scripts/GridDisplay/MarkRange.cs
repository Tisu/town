﻿using System.Collections.Generic;
using Assets.Scripts.GridDisplay;
using Assets.Scripts.GridFunctionProviders;
using Assets.Scripts.HexGrid.Hex;
using UnityEngine;

public sealed class MarkRange : IMarkRange
{
    readonly List<GridCellInfo> m_marked_grid = new List<GridCellInfo>();
    private readonly ISpecialMark m_special_special_mark;

    public MarkRange(ISpecialMark special_special_mark = null)
    {
        m_special_special_mark = special_special_mark;
    }

    public void MarkDistance(GameObject game_object, int distance)
    {
        var hex_grid = game_object.GetComponent<GridCellInfo>().m_hex_grid;
        MarkDistance(hex_grid, distance);
    }

    public void MarkDistance(HexGrid hex_grid, int distance)
    {
        hex_grid.ExecuteActionOnGridsInRange(distance, MarkGrid);
    }
    public void RemoveMarkedDistance()
    {
        foreach(var item in m_marked_grid)
        {
            item.SetSpriteColorNormal();
        }
        m_marked_grid.Clear();
    }

    private void MarkGrid(GridCellInfo grid_cell_info)
    {
        if(m_special_special_mark != null && m_special_special_mark.MarkSpecialRange(grid_cell_info))
        {
            m_marked_grid.Add(grid_cell_info);
            return;
        }

        grid_cell_info.SetSpriteColorRangeDisplay();
        m_marked_grid.Add(grid_cell_info);
    }
}