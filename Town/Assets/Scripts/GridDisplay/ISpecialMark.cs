﻿namespace Assets.Scripts.GridDisplay
{
    public interface ISpecialMark
    {
        bool MarkSpecialRange(GridCellInfo grid);
    }
}