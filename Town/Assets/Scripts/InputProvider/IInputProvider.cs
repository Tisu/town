﻿using UnityEngine;

public interface IInputProvider
{
    void SubscribeRunner(string tag, IHitRunner action_click_runner);
}