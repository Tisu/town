﻿using Assets.Scripts.Cheats;
using Assets.Scripts.EventSystem;
using Assets.Scripts.MainMenu;
using Commands;
using UnityEngine;

public sealed class InputProvider : MonoBehaviour, IInputProvider
{
    private readonly ICommandManager m_command_manager = new CommandManager();
    [SerializeField]
    private CustomStandaloneInputModule m_input_module;
    [SerializeField]
    private MainCamera m_main_camera;
    [SerializeField]
    private CheatManager m_cheat_manager;
    [SerializeField]
    private MenuInGame m_menu_in_game;
    void Update()
    {
        ListenCancelButtonInput();
        if(CheatInput())
        {
            return;
        }
        CameraUpdate();

        if (UserInteractionUpdate())
        {
            return;
        }
    }

    private bool UserInteractionUpdate()
    {
#if UNITY_STANDALONE
        var hit = GetMouseHit();
        if(hit == null)
        {
            return true;
        }
        RunMousePositionHit(hit);
        ActionClickInput(hit);
#elif UNITY_ANDROID
        if(Input.touches.Length > 0)
        {
            var touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                var touch_position = Camera.main.ScreenToWorldPoint(touch.position);
                var hit = Physics2D.OverlapPoint(touch_position);
                RunActionClick(hit);
            }
        }
#endif
        return false;
    }
    private void CameraUpdate()
    {
        m_main_camera.UpdateCameraPosition();
    }
    private bool CheatInput()
    {
        if(CheckIfConsoleCanBeOpen())
        {
            m_cheat_manager.OnTyldaClick();
            return true;
        }
        if(CheckIfConsoleShouldBeClosed())
        {
            m_cheat_manager.Cancel();
            return true;
        }
        if(CheckIfConsoleIsActive())
        {
            if(Input.GetButtonDown("Cancel"))
            {
                m_cheat_manager.Cancel();
                return true;
            }
            m_cheat_manager.Update();
            return true;
        }
        return false;
    }
    private bool CheckIfConsoleIsActive()
    {
        return m_cheat_manager.IsActive();
    }
    private bool CheckIfConsoleShouldBeClosed()
    {
        return Input.GetKeyDown(KeyCode.BackQuote) && m_cheat_manager.IsActive();
    }
    private bool CheckIfConsoleCanBeOpen()
    {
        return Input.GetKeyDown(KeyCode.BackQuote) && !m_cheat_manager.IsActive();
    }
    private void RunMousePositionHit(Collider2D hit)
    {
        m_command_manager.RunMousePositionHit(hit);
    }
    private void ActionClickInput(Collider2D hit)
    {
        if(Input.GetMouseButtonDown(MouseMapper.ACTION_CLICK))
        {
            RunActionClick(hit);
        }
    }
    private Collider2D GetMouseHit()
    {
        if(m_input_module.IsPointedOverInventory())
        {
            return null;
        }

        var world_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var hit = Physics2D.OverlapPoint(world_pos);
        return hit;
    }
    private void ListenCancelButtonInput()
    {
        if (!Input.GetButtonDown("Cancel"))
        {
            return;
        }

        if (!m_command_manager.InterruptExecution())
        {
            m_menu_in_game.EscButtonDown();
        }
    }
    public void SubscribeRunner(string tag, IHitRunner action_click_runner)
    {
        m_command_manager.SubscribeRunner(tag, action_click_runner);
    }
    public void RunActionClick(Collider2D hit)
    {
        if (CheckIfConsoleIsActive())
        {
            return;
        }
        m_command_manager.RunActionClick(hit);
    }
}