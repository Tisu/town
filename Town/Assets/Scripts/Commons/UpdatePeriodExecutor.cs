﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Commons
{
    public sealed class UpdatePeriodExecutor : IUpdatePeriodExecutor
    {
        private const float POSTPONE_TIME_CONVERSION = 10f;
        private const int MAX_POSTPONE_TIME = 3;

        private float m_last_executed_time;
        private readonly float m_executing_pause_period;
        private readonly Action m_method;
        public UpdatePeriodExecutor(float executing_pause_period, Action method, MonoBehaviour mono)
        {
            m_executing_pause_period = executing_pause_period;
            m_method = method;
            m_last_executed_time = Time.time;
            if(mono != null)
            {
                mono.StartCoroutine(PostPoneFirstJob());
            }
        }
        public UpdatePeriodExecutor(float executing_pause_period, Action method, MonoBehaviour mono, int postpone_time)
        {
            m_executing_pause_period = executing_pause_period;
            m_method = method;
            m_last_executed_time = Time.time;
            mono.StartCoroutine(PostPoneFirstJob(postpone_time));
        }

        public void Update()
        {
            if (GamePause.GamePause.m_is_game_paused)
            {
                return;
            }
            if(m_last_executed_time + m_executing_pause_period + GamePause.GamePause.m_pause_duration < Time.time)
            {
                m_method();
                m_last_executed_time = Time.time;
            }
        }

        private IEnumerator PostPoneFirstJob(int postpone_time)
        {
            yield return new WaitForSeconds(postpone_time);
            m_method();
        }
        private IEnumerator PostPoneFirstJob()
        {
            var predicted_time = m_executing_pause_period / POSTPONE_TIME_CONVERSION;
            var postpone_time = predicted_time > MAX_POSTPONE_TIME ? MAX_POSTPONE_TIME : predicted_time;
            yield return new WaitForSeconds(postpone_time);
            m_method();
        }
    }
}