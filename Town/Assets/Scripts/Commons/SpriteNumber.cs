﻿using System.Linq;

namespace Assets.Scripts.Commons
{
    public static class SpriteNumber
    {
        public static int GetNumber(GridCellInfo sprite_cell_info)
        {
            return GetNumber(sprite_cell_info.GetSpriteName());
        }

        public static int GetNumber(string sprite_name)
        {
            var name = sprite_name.Split('_');
            var number = 0;
            if(name.Length <= 1)
            {
                return number;
            }

            if(!int.TryParse(name.LastOrDefault(), out number))
            {
                number = 0;
            }

            return number;
        }
    }
}