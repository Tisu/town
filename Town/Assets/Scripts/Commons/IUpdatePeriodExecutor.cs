﻿namespace Assets.Scripts.Commons
{
    public interface IUpdatePeriodExecutor
    {
        void Update();
    }
}