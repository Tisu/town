﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.MainMenu
{
    public sealed class MainMenuManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_settings;
        [SerializeField]
        private GameObject m_main_menu;

        public void Exit()
        {
            Application.Quit();
        }

        public void Settings()
        {
            m_main_menu.SetActive(false);
            m_settings.SetActive(true);
        }

        public void MainButtons()
        {
            m_main_menu.SetActive(true);
            m_settings.SetActive(false);
        }

        public void Play()
        {
            SceneManager.LoadScene("Main");
        }
    }
}