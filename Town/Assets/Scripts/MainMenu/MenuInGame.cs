﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.MainMenu
{
    public enum MenuState
    {
        inactive,
        settings,
        main_buttons,
        game_over
    }
    public sealed class MenuInGame : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_settings;
        [SerializeField]
        private GameObject m_main_menu;
        [SerializeField]
        private Slider m_volume_slider;
        [SerializeField]
        private Toggle m_mute_volume;

        [SerializeField]
        private InputProvider m_input_provider;
        private MenuState m_menu_state;
        private float m_menu_open_time;

        void Start()
        {
            var sound_manager = GameObject.Find("SoundManager").GetComponent<SoundManager.SoundManager>();
            m_volume_slider.onValueChanged.AddListener(sound_manager.OnSliderValueChange);
            m_mute_volume.onValueChanged.AddListener(sound_manager.OnMuteClickChange);
        }
        public void Exit()
        {
            Application.Quit();
        }

        public void Settings()
        {
            m_menu_state = MenuState.settings;
            m_main_menu.SetActive(false);
            m_settings.SetActive(true);
        }

        public void MainButtons()
        {
            m_menu_state = MenuState.main_buttons;
            m_main_menu.SetActive(true);
            m_settings.SetActive(false);
        }

        public void EscButtonDown()
        {
            switch(m_menu_state)
            {
                case MenuState.inactive:
                    m_menu_open_time = Time.time;
                    MainButtons();
                    GamePause.GamePause.m_is_game_paused = true;
                    break;
                case MenuState.settings:
                    MainButtons();
                    break;
                case MenuState.main_buttons:
                    m_menu_state = MenuState.inactive;
                    m_main_menu.SetActive(false);
                    GamePause.GamePause.m_pause_duration = Time.time - m_menu_open_time;
                    GamePause.GamePause.m_is_game_paused = false;
                    break;
                case MenuState.game_over:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public void Resume()
        {
            m_menu_state = MenuState.main_buttons;
            EscButtonDown();
        }

        public void GameOver()
        {
            GamePause.GamePause.m_is_game_paused = true;
            m_main_menu.SetActive(true);
            foreach(Transform button in m_main_menu.transform)
            {
                if (button.name == "Exit" || button.name == "MainMenu")
                {
                    continue;
                }
                button.gameObject.SetActive(false);
            }
        }

        public void MainMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}