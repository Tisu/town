using UnityEngine;

namespace CustomCamera
{
    public interface IValidateMovementController
    {
        void MoveWithValidation(Transform transform);
        Vector2 m_max_position { get; set; }
    }
}