﻿using UnityEngine;

namespace CustomCamera
{
    public interface IRotationController
    {
        void Rotate(Transform camera_transform);
        void SetStartRotation(Vector3 start_rotation);
    }
}