﻿using UnityEngine;

namespace CustomCamera
{
    public interface IValidateZoomingController
    {
        void ZoomWithValidation(Transform camera_transform);
        void AdjustStartZoom(Transform camera_transform);
    }
}