﻿using UnityEngine;

namespace CustomCamera
{
    public interface IMovementController
    {
        void Move(Transform transform);
    }
}