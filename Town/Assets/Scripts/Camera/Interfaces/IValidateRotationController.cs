﻿using UnityEngine;

namespace CustomCamera
{
    public interface IValidateRotationController
    {
        void RotateWithRestriction(Transform camera_transform);
        void SetStartRotation(Vector3 start_rotation);
    }
}