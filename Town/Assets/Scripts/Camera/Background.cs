﻿using UnityEngine;

public class Background : MonoBehaviour
{
    const float PIXEL_2_UNIT = 3000F;
    void Awake()
    {
        transform.localScale = Tisu.CurrentScreenBorderPositionOrthographic.m_screen_right_up_border * PIXEL_2_UNIT;
    }
}
