﻿using CustomCamera;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public const float MIN_ZOOMING = 1;
    public const float MAX_ZOOMING = 15;

    [SerializeField]
    private Vector2 m_max_position;
    [SerializeField]
    float m_movement_speed = 1f;
    [SerializeField]
    private float m_zooming_speed = 10f;

    private IValidateZoomingController m_zooming_controller;
    private IValidateMovementController m_movement_controller;

    void Awake()
    {
        m_zooming_controller = new ZoomingController(m_zooming_speed);
        m_movement_controller = new MovementController(m_movement_speed, m_max_position);
    }

    void OnEnable()
    {
        m_zooming_controller = m_zooming_controller ?? new ZoomingController(m_zooming_speed);
        m_zooming_controller.AdjustStartZoom(transform);
    }
    public void UpdateCameraPosition()
    {
        CaptureMovementKeys();
        Zooming();
    }

    public void SetMaxCameraPosition(Vector2 camera_pos)
    {
        m_movement_controller.m_max_position = camera_pos;
    }

    public float GetMaxPositionX()
    {
        return m_movement_controller.m_max_position.x;
    }

    public float GetMaxPositionY()
    {
        return m_movement_controller.m_max_position.y;
    }
    private void CaptureMovementKeys()
    {
        m_movement_controller.MoveWithValidation(transform);
    }
    private void Zooming()
    {
        m_zooming_controller.ZoomWithValidation(transform);
    }
}

