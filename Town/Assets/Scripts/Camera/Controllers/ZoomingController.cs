﻿
using UnityEngine;

namespace CustomCamera
{
    public class ZoomingController : IValidateZoomingController
    {
        private const float MAX_ZOOMING_THRESHOLD = 0.5f;
        private const string MOUSE_SCROLLWHEEL = "Mouse ScrollWheel";

        private readonly float m_zooming_speed;
        public ZoomingController(float zooming_speed)
        {
            m_zooming_speed = zooming_speed;
        }

        public void ZoomWithValidation(Transform camera_transform)
        {
            ZoomingKeysWithValidation(camera_transform);
            ZoomingWithValidation(camera_transform);
        }
        public void AdjustStartZoom(Transform camera_transform)
        {
            if(camera_transform.transform.position.y > MainCamera.MAX_ZOOMING)
            {
                camera_transform.transform.position =
                    new Vector3(camera_transform.transform.position.x,
                    MainCamera.MAX_ZOOMING - MAX_ZOOMING_THRESHOLD,
                    camera_transform.transform.position.z);
            }
        }

        private void ZoomingWithValidation(Transform camera_transform)
        {
            if(Input.GetAxis(MOUSE_SCROLLWHEEL) > 0 && ValidateZoomingForward())
            {
                ZoomIn();
            }
            else if(Input.GetAxis(MOUSE_SCROLLWHEEL) < 0 && ValidateZoomingBackward())
            {
                ZoomOut();
            }
        }

        private void ZoomIn()
        {
            Camera.main.orthographicSize -= m_zooming_speed;
        }
        private void ZoomOut()
        {
            Camera.main.orthographicSize += m_zooming_speed;
        }

        private void ZoomingKeysWithValidation(Transform camera_transform)
        {
            if(Input.GetKey(KeyMapper.CAMERA_ZOOM_IN) && ValidateZoomingForward())
            {
                ZoomIn();
            }
            if(Input.GetKey(KeyMapper.CAMERA_ZOOM_OUT) && ValidateZoomingBackward())
            {
                ZoomOut();
            }
        }
        public bool ValidateZoomingForward()
        {
            return Camera.main.orthographicSize > MainCamera.MIN_ZOOMING;
        }

        public bool ValidateZoomingBackward()
        {
            return Camera.main.orthographicSize < MainCamera.MAX_ZOOMING;
        }
    }
}