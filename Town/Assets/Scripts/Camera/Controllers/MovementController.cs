﻿
using System;
using UnityEngine;

namespace CustomCamera
{
    public class MovementController : IMovementController, IValidateMovementController
    {
        private readonly float m_movement_speed;
        public Vector2 m_max_position { get; set; }
        private Vector3 m_predicted_position;

        public MovementController(float movement_speed)
        {
            m_movement_speed = movement_speed;
        }

        public MovementController(float movement_speed, Vector2 max_position)
        {
            m_movement_speed = movement_speed;
            m_max_position = max_position;
        }
        public void Move(Transform transform)
        {
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_UP))
            {
                transform.Translate(new Vector3(0, m_movement_speed, 0) * Time.deltaTime);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_LEFT))
            {
                transform.Translate(new Vector3(-m_movement_speed, 0, 0) * Time.deltaTime);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_RIGHT))
            {
                transform.Translate(new Vector3(m_movement_speed, 0, 0) * Time.deltaTime);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_DOWN))
            {
                transform.Translate(new Vector3(0, -m_movement_speed, 0) * Time.deltaTime);
            }
        }
        public void MoveWithValidation(Transform transform)
        {
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_UP))
            {
                m_predicted_position = transform.position + transform.up * m_movement_speed;
                AssignMove(transform);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_DOWN))
            {
                m_predicted_position = transform.position - transform.up * m_movement_speed;
                AssignMove(transform);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_LEFT))
            {
                m_predicted_position = transform.position - transform.right * m_movement_speed;
                AssignMove(transform);
            }
            if(Input.GetKey(KeyMapper.CAMERA_MOVE_RIGHT))
            {
                m_predicted_position = transform.position + transform.right * m_movement_speed;
                AssignMove(transform);
            }
        }
        private void AssignMove(Transform camera_transform)
        {
            if(ValidateMove())
            {
                camera_transform.transform.position = m_predicted_position;
            }
        }
        private bool ValidateMove()
        {
            return m_predicted_position.x < m_max_position.x
                   && m_predicted_position.x > -m_max_position.x
                   && m_predicted_position.y < m_max_position.y
                   && m_predicted_position.y > -m_max_position.y;
        }
    }
}