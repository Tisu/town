﻿
using UnityEngine;

namespace CustomCamera
{
    public class RotationController : IRotationController, IValidateRotationController
    {
        private Vector2 m_rotation_update;

        private Vector3 m_start_rotation;
        private readonly float m_degree_speed;
        private readonly float m_max_panning;

        public RotationController(float degree_speed, Vector3 start_rotation)
        {
            m_degree_speed = degree_speed;
            m_start_rotation = start_rotation;
        }

        public RotationController(float degree_speed, Vector3 start_rotation, float max_panning)
        {
            m_degree_speed = degree_speed;
            m_start_rotation = start_rotation;
            m_max_panning = max_panning;
        }

        public void Rotate(Transform camera_transform)
        {
            if (Input.GetMouseButton(MouseMapper.CAMERA_ROTATION))
            {
                UpdateRotation();
                camera_transform.localEulerAngles = new Vector3(m_rotation_update.x + m_start_rotation.x, -m_rotation_update.y + m_start_rotation.y, 0);
            }
        }

        public void RotateWithRestriction(Transform camera_transform)
        {
            if (Input.GetMouseButton(MouseMapper.CAMERA_ROTATION))
            {
                UpdateRotation();
                RestrictRotation();
                camera_transform.localEulerAngles = new Vector3(m_rotation_update.x + m_start_rotation.x, -m_rotation_update.y + m_start_rotation.y, 0);
            }
        }

        private void UpdateRotation()
        {
            m_rotation_update.x -= Input.GetAxis("Mouse Y") * m_degree_speed * Time.deltaTime;
            m_rotation_update.y -= Input.GetAxis("Mouse X") * m_degree_speed * Time.deltaTime;
        }

        private void RestrictRotation()
        {
            m_rotation_update.x = Mathf.Clamp(m_rotation_update.x, -m_max_panning, m_max_panning);
        }

        public void SetStartRotation(Vector3 start_rotation)
        {
            m_start_rotation = start_rotation;
        }
    }
}