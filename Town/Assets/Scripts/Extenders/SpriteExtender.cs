﻿using System;
using System.Linq;
using Assets.Scripts.AssetsManagement.Caches.Building;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts.SpriteExtender
{
    public static class SpriteExtender
    {

        public static bool IsDependent(this Sprite sprite, string dependent)
        {
            return sprite.StartsWith(dependent);
        }

        public static bool IsResource(this Sprite sprite)
        {
            return ObjectType.RESOURCE.Any(x => sprite.name.StartsWith(x));
        }
        public static bool IsWood(this Sprite sprite)
        {
            return sprite.name.StartsWith(ObjectType.WOOD);
        }
        public static bool IsStone(this Sprite sprite)
        {
            return sprite.name.StartsWith(ObjectType.STONE);
        }
        public static string GetObjectType(this Sprite sprite)
        {
            foreach(var name in ObjectType.ARRAY)
            {
                if(sprite.StartsWith(name))
                {
                    return name;
                }
            }
            return ObjectType.UNDEFINED;
        }

        public static bool StartsWith(this Sprite sprite, string name)
        {
            return sprite.name.StartsWith(name.ToLower());
        }
    }
}