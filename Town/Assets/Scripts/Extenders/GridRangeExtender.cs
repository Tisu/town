﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GridFunctionProviders
{
    public static class GridRangeExtender
    {
        public static void ExecuteActionOnGridsInRange(this HexGrid.Hex.HexGrid start_grid, int distance, Action<GridCellInfo> action)
        {
            for(var dx = -distance; dx <= distance; dx++)
            {
                for(var dy = Mathf.Max(-distance, -dx - distance); dy <= Mathf.Min(distance, -dx + distance); dy++)
                {
                    var dz = -dx - dy;
                    var hex_grid = new HexGrid.Hex.HexGrid(dx + start_grid.m_x, dy + start_grid.m_y, dz + start_grid.m_z);

                    var grid_cell_info = GridGameController.m_instance.GetCellInfo(hex_grid);
                    if(grid_cell_info == null || grid_cell_info.IsBusy())
                    {
                        continue;
                    }
                    action(grid_cell_info);
                }
            }
        }
        public static void ExecuteOnlyOnceActionOnGridsInRange(this HexGrid.Hex.HexGrid start_grid, int distance, Func<GridCellInfo,bool> action)
        {
            for(var dx = -distance; dx <= distance; dx++)
            {
                for(var dy = Mathf.Max(-distance, -dx - distance); dy <= Mathf.Min(distance, -dx + distance); dy++)
                {
                    var dz = -dx - dy;
                    var hex_grid = new HexGrid.Hex.HexGrid(dx + start_grid.m_x, dy + start_grid.m_y, dz + start_grid.m_z);

                    var grid_cell_info = GridGameController.m_instance.GetCellInfo(hex_grid);
                    if(grid_cell_info == null || grid_cell_info.IsBusy())
                    {
                        continue;
                    }
                    if (action(grid_cell_info))
                    {
                        return;
                    }
                }
            }
        }
        public static List<GridCellInfo> GetGrinds(this HexGrid.Hex.HexGrid start_grid, int distance, Func<GridCellInfo, bool> add_filter)
        {
            var list = new List<GridCellInfo>();
            for(var dx = -distance; dx <= distance; dx++)
            {
                for(var dy = Mathf.Max(-distance, -dx - distance); dy <= Mathf.Min(distance, -dx + distance); dy++)
                {
                    var dz = -dx - dy;
                    var hex_grid = new HexGrid.Hex.HexGrid(dx + start_grid.m_x, dy + start_grid.m_y, dz + start_grid.m_z);

                    var grid_cell_info = GridGameController.m_instance.GetCellInfo(hex_grid);
                    if(grid_cell_info == null || grid_cell_info.IsBusy())
                    {
                        continue;
                    }
                    if (add_filter(grid_cell_info))
                    {
                        list.Add(grid_cell_info);
                    }
                }
            }
            return list;
        }
    }
}