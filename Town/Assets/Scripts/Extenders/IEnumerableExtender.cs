﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace Assets.Scripts.SpriteExtender
{
    public static class IEnumerableExtender
    {
        public static T Random<T>(this IEnumerable<T> collection)
        {
            var count = collection.Count();
            var random = UnityEngine.Random.Range(0, count);
            return count == 0 ? default(T) : collection.ElementAt(random);
        }
        public static T RandomAndIsNot<T>(this IEnumerable<T> collection, T excluded_element)
        {
            Assert.AreNotEqual(collection.Count(), 1);
            var collection_with_excluded = collection.Where(x => !x.Equals(excluded_element));
            var count = collection_with_excluded.Count();
            var random = UnityEngine.Random.Range(0, count);
            return count == 0 ? default(T) : collection_with_excluded.ElementAt(random);
        }
    }
}