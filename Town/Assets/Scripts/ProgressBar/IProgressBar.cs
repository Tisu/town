﻿using System;
using UnityEngine;

namespace Assets.Scripts.ProgressBar
{
    public interface IProgressBar
    {
        GridCellInfo m_grid_cell_traget { get; set; }
        Action m_on_finished { get; set; }
        float m_progress_time { get; set; }

        void DontDestroyOnFinish();
        void SetProgressBarColor(Color32 color);
        void SetText(string text);
    }
}