﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ProgressBar
{
    public sealed class ProgressBar : MonoBehaviour, IProgressBar
    {
        [SerializeField]
        private Text m_text;
        [SerializeField]
        private Image m_progress_bar;
        [SerializeField]
        private Text m_display_text;

        public Action m_on_finished { get; set; }
        public float m_progress_time
        {
            get
            {
                return _progress_time;
            }
            set
            {
                if (value <= 0)
                {
                    _progress_time = float.Epsilon;
                    m_progress_bar.fillAmount = 1;
                    return;
                }
                _progress_time = value;
                m_progress_bar.fillAmount = 0;
                m_progress_per_tick = 1 / (m_progress_time / Time.fixedDeltaTime);
            }
        }

        public GridCellInfo m_grid_cell_traget
        {
            get
            {
                return _target_grid;
            }
            set
            {
                _target_grid = value;
                _target_grid.m_process_bar = this;
            }
        }
        private GridCellInfo _target_grid;
        private float _progress_time;
        private float m_progress_per_tick;
        private bool m_destroy_after_finished = true;
        void FixedUpdate()
        {
            if (GamePause.GamePause.m_is_game_paused)
            {
                return;
            }
            if(m_progress_time <= 0)
            {
                return;
            }
            m_progress_bar.fillAmount += m_progress_per_tick;
            m_text.text = ((int)(m_progress_bar.fillAmount * 100)) + " %";
            if(m_progress_bar.fillAmount >= 1)
            {
                if(m_on_finished != null)
                {
                    m_on_finished();
                }
                _target_grid.m_process_bar = null;
                if(m_destroy_after_finished)
                {
                    Destroy(gameObject);
                }
            }
        }

        public void SetText(string text)
        {
            m_display_text.text = text;
        }
        public void SetProgressBarColor(Color32 color)
        {
            m_progress_bar.color = color;
        }
        public void DontDestroyOnFinish()
        {
            m_destroy_after_finished = false;
        }
    }
}