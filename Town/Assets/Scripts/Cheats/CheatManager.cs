﻿using System;
using System.Collections.Generic;
using Assets.Scripts.AssetsManagement.Caches.Building;
using Assets.Scripts.Buildings;
using Assets.Scripts.CityHall_Unlock;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cheats
{
    public sealed class CheatManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_cheat_canvas;
        [SerializeField]
        private InputField m_input_field;
        [SerializeField]
        private GameObject m_unlock_canvas;
        private readonly Dictionary<string, Action> m_cheats_actions = new Dictionary<string, Action>()
        {
            {CheatsCommands.INSTA_BUILDING, InstaBuildingCheat},
            {CheatsCommands.GET_RESOURCES, GetResources},
            {CheatsCommands.GET_FOOD,GetFood },
            {CheatsCommands.STARVATION,Starvation }
        };
        void Awake()
        {
            m_cheats_actions.Add(CheatsCommands.UNLOCK_ALL, UnlockAllBuildings);
        }
        public void OnTyldaClick()
        {
            m_cheat_canvas.SetActive(true);
            m_input_field.ActivateInputField();
        }
        public bool IsActive()
        {
            return m_cheat_canvas.activeSelf;
        }
        public void Cancel()
        {
            m_cheat_canvas.SetActive(false);
        }
        public void Update()
        {
            if(!Input.GetButtonDown("Submit"))
            {
                return;
            }
            Action cheat;
            if(m_cheats_actions.TryGetValue(m_input_field.text, out cheat))
            {
                cheat();
            }
            m_input_field.text = "";
            m_input_field.ActivateInputField();
        }
        private void UnlockAllBuildings()
        {
            var managers = m_unlock_canvas.GetComponentsInChildren<UnlockDisplayManager>();
            foreach(var manager in managers)
            {
                manager.SetUnlockCheat();
            }
        }
        private static void GetResources()
        {
            ResourcesManager.m_instance.m_wood += 100;
            ResourcesManager.m_instance.m_stone += 100;
        }
        private static void InstaBuildingCheat()
        {
            foreach(BuildingInformation building in BuildingsLoadedCache.m_cache)
            {
                building.m_building_time = 0;
            }
        }
        private static void GetFood()
        {
            ResourcesManager.m_instance.m_food += 100;
        }
        private static void Starvation()
        {
            ResourcesManager.m_instance.m_food = -10;
        }
    }
}