﻿namespace Assets.Scripts.Cheats
{
    public static class CheatsCommands
    {
        public const string INSTA_BUILDING = "instabuilding";
        public const string GET_RESOURCES = "getresources";
        public const string UNLOCK_ALL = "unlockall";
        public const string GET_FOOD = "getfood";
        public const string STARVATION = "starve";
    }
}