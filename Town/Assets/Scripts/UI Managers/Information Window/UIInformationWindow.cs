﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI_Managers
{
    public sealed class UIInformationWindow : MonoBehaviour
    {
        [SerializeField]
        private Text m_main;
        [SerializeField]
        private Text m_description;
        [SerializeField]
        private UIResourcesManager m_information_window_resources;
        [SerializeField]
        private GameObject m_information_window;

        private IBuildingInformation m_current_building_information;
        public void DisableInformationWindow()
        {
            m_information_window.SetActive(false);
            ResourcesManager.m_instance.m_change_resources_callback -= UpdateUI;
        }
        public void SetBuildingInformation(IBuildingInformation building_information)
        {
            if(building_information != null)
            {
                m_current_building_information = building_information;
                UpdateUI();
                ResourcesManager.m_instance.m_change_resources_callback += UpdateUI;
            }
        }

        private void UpdateUI()
        {
            m_information_window.SetActive(true);
            SetDescription(m_current_building_information.GetDescription());
            SetWoodValue(m_current_building_information.GetWoodCost());
            SetStoneValue(m_current_building_information.GetStoneCost());
            SetMainText(m_current_building_information.GetName());
        }
        private void SetMainText(string text)
        {
            m_main.text = text;
        }
        private void SetDescription(string text)
        {
            m_description.text = text;
        }
        private void SetWoodValue(uint wood)
        {
            m_information_window_resources.SetWood(wood);
            if (ResourceChecker.CheckIfEnoughtWood(wood))
            {
                m_information_window_resources.SetWoodNormal();
            }
            else
            {
                m_information_window_resources.SetNotEnoughWood();
            }
        }
        private void SetStoneValue(uint stone)
        {
            m_information_window_resources.SetStone(stone);
            if(ResourceChecker.CheckIfEnoughtStone(stone))
            {
                m_information_window_resources.SetStoneNormal();
            }
            else
            {
                m_information_window_resources.SetNotEnoughStone();
            }
        }
    }
}