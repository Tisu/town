﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI_Managers.Market
{
    [RequireComponent(typeof(Slider))]
    [RequireComponent(typeof(UIResourcesManager))]
    public sealed class WoodToStoneExecutor : MonoBehaviour
    {
        private Slider m_slider;
        private UIResourcesManager m_resources_manager;

        void Awake()
        {
            m_slider = GetComponent<Slider>();
            m_resources_manager = GetComponent<UIResourcesManager>();
        }
        public void OnSliderValueChange()
        {
            var wood = CalculateWood();
            m_resources_manager.SetWood(wood);
            m_resources_manager.SetStone(CalculateWoodToStone(wood));
        }
        public void OnExchangeButtonClick()
        {
            var wood = CalculateWood();
            ResourcesManager.m_instance.m_wood -= wood;
            ResourcesManager.m_instance.m_stone += CalculateWoodToStone(wood);
            m_slider.value = 0;
        }
        private uint CalculateWood()
        {
            var wood = (uint)(m_slider.value * ResourcesManager.m_instance.m_stone);
            return wood - wood % ResourceExchangeRatio.WOOD_TO_STONE;
        }

        private uint CalculateWoodToStone(uint wood)
        {
            return wood / ResourceExchangeRatio.WOOD_TO_STONE;
        }
    }
}