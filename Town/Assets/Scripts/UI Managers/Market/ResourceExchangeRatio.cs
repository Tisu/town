﻿namespace Assets.Scripts.UI_Managers.Market
{
    public static class ResourceExchangeRatio
    {
        public const int STONE_TO_WOOD = 3;
        public const int WOOD_TO_STONE = 3;
    }
}