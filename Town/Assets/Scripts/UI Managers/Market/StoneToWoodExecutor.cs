﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI_Managers.Market
{
    public sealed class StoneToWoodExecutor : MonoBehaviour
    {
        [SerializeField]
        private Slider m_slider;
        [SerializeField]
        private UIResourcesManager m_resources_manager;
   
        public void OnSliderValueChange()
        {
            var stone = CalculateStone();
            m_resources_manager.SetStone(stone);
            m_resources_manager.SetWood(CalculateWood(stone));
        }
        public void OnExchangeButtonClick()
        {
            var stone = CalculateStone();
            ResourcesManager.m_instance.m_stone -= stone;
            ResourcesManager.m_instance.m_wood += CalculateWood(stone);
            m_slider.value = 0;
        }
        private uint CalculateStone()
        {
            var stone = (uint)(m_slider.value * ResourcesManager.m_instance.m_stone);
            return stone - stone % ResourceExchangeRatio.STONE_TO_WOOD;
        }

        private uint CalculateWood(uint stone)
        {
            return stone / ResourceExchangeRatio.STONE_TO_WOOD;
        }
    }
}