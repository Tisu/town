﻿using System;

namespace Assets.Scripts.CityHall_Unlock
{
    public sealed class UIResourceDisplay : IDisposable
    {
        private uint _wood_cost;
        private uint _stone_cost;
        private readonly UIResourcesManager m_resources;

        public uint m_wood_cost
        {
            get { return _wood_cost; }
            set
            {
                _wood_cost = value;
                OnResourceChange();
            }
        }
        public uint m_stone_cost
        {
            get { return _stone_cost; }
            set { _stone_cost = value; OnResourceChange(); }
        }

        public UIResourceDisplay(UIResourcesManager resources, uint wood_cost, uint stone_cost)
        {
            m_resources = resources;
            m_wood_cost = wood_cost;
            m_stone_cost = stone_cost;
            ResourcesManager.m_instance.m_change_resources_callback += OnResourceChange;
            OnResourceChange();
        }

        public void Dispose()
        {
            ResourcesManager.m_instance.m_change_resources_callback -= OnResourceChange;
        }
        private void OnResourceChange()
        {
            ChangeWoodDisplay();
            ChangeStoneDisplay();
        }

        private void ChangeStoneDisplay()
        {
            if(ResourceChecker.CheckIfEnoughtStone(m_stone_cost))
            {
                m_resources.SetStoneNormal();
            }
            else
            {
                m_resources.SetNotEnoughStone();
            }
        }

        private void ChangeWoodDisplay()
        {
            if(ResourceChecker.CheckIfEnoughtWood(m_wood_cost))
            {
                m_resources.SetWoodNormal();
            }
            else
            {
                m_resources.SetNotEnoughWood();
            }
        }
    }
}