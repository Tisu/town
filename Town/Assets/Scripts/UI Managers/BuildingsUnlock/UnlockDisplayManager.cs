﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.CityHall_Unlock
{
    public sealed class UnlockDisplayManager : MonoBehaviour
    {
        [SerializeField]
        private Text m_display_text;
        [SerializeField]
        private Button m_button;
        [SerializeField]
        private uint m_wood_cost;
        [SerializeField]
        private uint m_stone_cost;
        [SerializeField]
        private UIResourcesManager m_resources;

        [SerializeField]
        private Button m_next_to_unlock;
        [SerializeField]
        private InventoryMono m_inventory;
        private UIResourceDisplay m_ui_resource_display;
        
        void Awake()
        {
            m_resources.SetWood(m_wood_cost);
            m_resources.SetStone(m_stone_cost);
            m_ui_resource_display = new UIResourceDisplay(m_resources,m_wood_cost,m_stone_cost);
        }

        void OnValidate()
        {
            m_resources.SetWood(m_wood_cost);
            m_resources.SetStone(m_stone_cost);
        }

        void OnDestroy()
        {
            m_ui_resource_display.Dispose();
        }
        public void SetUnlocked()
        {
            if (!ResourceChecker.CheckIfEnoughtResources(m_wood_cost,m_stone_cost))
            {
                return;
            }
            Unlock();
            if(m_next_to_unlock != null)
            {
                m_next_to_unlock.interactable = true;
            }
            else
            {
                Debug.LogError("Next to unlock is null");
            }
        }

        public void SetUnlockCheat()
        {
            Unlock();
        }

        public void DecreaseCost(uint wood, uint stone)
        {
            var wood_after_change = m_wood_cost - wood;
            if (wood_after_change >= 0 )
            {
                m_wood_cost = wood_after_change;
            }

            var stone_after_change = m_stone_cost - stone;
            if(stone_after_change >= 0)
            {
                m_stone_cost = stone_after_change;
            }
        }
        private void Unlock()
        {
            m_button.interactable = false;
            m_button.colors = new ColorBlock
            {
                disabledColor = Color.white,
                normalColor = Color.white,
                pressedColor = Color.white,
                colorMultiplier = 1,
                highlightedColor = Color.white
            };
            m_display_text.text = "";
            m_inventory.MountItem(GetComponent<Image>().sprite.name);
        }
    }
}