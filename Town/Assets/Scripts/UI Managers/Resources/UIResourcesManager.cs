﻿
using Assets.Scripts.CityHall_Unlock;
using UnityEngine;
using UnityEngine.UI;

public sealed class UIResourcesManager : MonoBehaviour, IUIResourcesManager
{
    private static readonly Color32 ENOUGH_RESOURCES = Color.white;
    private static readonly Color32 NOT_ENOUGH_RESOURCES = Color.red;

    [SerializeField]
    private Text m_wood_display;
    [SerializeField]
    private Text m_stone_display;

    private UIResourceDisplay m_resource_display;
    public void SetWood(uint wood)
    {
        if(m_resource_display != null)
        {
            m_resource_display.m_wood_cost = wood;
        }
        m_wood_display.text = wood.ToString();
    }
    public void SetStone(uint stone)
    {
        if(m_resource_display != null)
        {
            m_resource_display.m_stone_cost = stone;
        }
        m_stone_display.text = stone.ToString();
    }
    public void SetNotEnoughWood()
    {
        m_wood_display.color = NOT_ENOUGH_RESOURCES;
    }
    public void SetNotEnoughStone()
    {
        m_stone_display.color = NOT_ENOUGH_RESOURCES;
    }
    public void SetWoodNormal()
    {
        m_wood_display.color = ENOUGH_RESOURCES;
    }
    public void SetStoneNormal()
    {
        m_stone_display.color = ENOUGH_RESOURCES;
    }

    public void SetNormal(Text display)
    {
        display.color = ENOUGH_RESOURCES;
    }
    public void SetNotEnoughResources(Text display)
    {
        display.color = NOT_ENOUGH_RESOURCES;
    }
    public void ManageResourceChanges(uint wood, uint stone)
    {
        SetStone(stone);
        SetWood(wood);
        m_resource_display = new UIResourceDisplay(this, wood, stone);
    }

    void OnDestroy()
    {
        if(m_resource_display != null)
        {
            m_resource_display.Dispose();
        }
    }
}
