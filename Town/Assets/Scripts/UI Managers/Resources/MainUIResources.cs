﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI_Managers.Resources
{
    public sealed class MainUIResources : MonoBehaviour
    {
        [SerializeField]
        private UIResourcesManager m_base_resources_manager;
        [SerializeField]
        private Text m_food_text;
        [SerializeField]
        private Text m_population_text;
        public void SetWood(uint wood)
        {
            m_base_resources_manager.SetWood(wood);
        }
        public void SetStone(uint stone)
        {
            m_base_resources_manager.SetStone(stone);
        }

        public void SetFood(int food)
        {
            m_food_text.text = food.ToString();
            if (food <= 0 )
            {
                m_base_resources_manager.SetNotEnoughResources(m_food_text);
            }
            else
            {
                m_base_resources_manager.SetNormal(m_food_text);
            }
        }

        public void SetPopulation(int population, int max_population)
        {
            m_population_text.text = string.Format("{0}/{1}", population.ToString(), max_population.ToString());
            PopulationDisplayChange();
        }

        private void SetNotEnoughPopulation()
        {
            m_base_resources_manager.SetNotEnoughResources(m_population_text);
        }
        private void SetNormalPopulation()
        {
            m_base_resources_manager.SetNormal(m_population_text);
        }
        private void PopulationDisplayChange()
        {
            if(ResourcesManager.m_instance.CheckIfEnoughPopulationForMainDisplay())
            {
                SetNormalPopulation();
            }
            else
            {
                SetNotEnoughPopulation();
            }
        }
    }
}