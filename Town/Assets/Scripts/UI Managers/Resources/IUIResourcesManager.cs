﻿public interface IUIResourcesManager
{
    void ManageResourceChanges(uint wood, uint stone);
    void SetNotEnoughStone();
    void SetNotEnoughWood();
    void SetStone(uint stone);
    void SetStoneNormal();
    void SetWood(uint wood);
    void SetWoodNormal();
}