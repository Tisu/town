﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public sealed class TerrainUnlockerInformation : MonoBehaviour
    {
        public Vector3 m_draw_origin { get; set; }
        public UIResourcesManager m_resources_manager { get; set; }
    }
}