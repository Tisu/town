﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public interface ICameraMaxPositionAdjuster
    {
        void ChangeCameraMaxPosition(Vector3 world_terrain_position);
    }
}