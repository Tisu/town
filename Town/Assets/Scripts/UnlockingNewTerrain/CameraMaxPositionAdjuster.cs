﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public sealed class CameraMaxPositionAdjuster : ICameraMaxPositionAdjuster
    {
        private readonly MainCamera m_main_camera;
        private readonly Vector3 m_camera_unlock_adjustment;

        public CameraMaxPositionAdjuster(MainCamera camera, SpawnGridMono grid_spawner)
        {
            m_main_camera = camera;
            m_camera_unlock_adjustment = GridFrameworkPositionProvider.m_instance.GridToWorld(new Vector3(grid_spawner.GetWidth(), grid_spawner.GetHeight(), 0));
        }
        public void ChangeCameraMaxPosition(Vector3 world_terrain_position)
        {
            if(Mathf.Abs(world_terrain_position.x) > Mathf.Abs(m_main_camera.GetMaxPositionX()))
            {
                m_main_camera.SetMaxCameraPosition(new Vector2(m_main_camera.GetMaxPositionX() + m_camera_unlock_adjustment.x + 1, m_main_camera.GetMaxPositionY())); ;
            }

            if(Mathf.Abs(world_terrain_position.y) > Mathf.Abs(m_main_camera.GetMaxPositionY()))
            {
                m_main_camera.SetMaxCameraPosition(new Vector2(m_main_camera.GetMaxPositionX(), m_main_camera.GetMaxPositionY() + m_camera_unlock_adjustment.y + 1));
            }
        }
    }
}