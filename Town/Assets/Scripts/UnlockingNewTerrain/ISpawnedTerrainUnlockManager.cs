﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public interface ISpawnedTerrainUnlockManager
    {
        void Add(TerrainUnlockerInformation manager, uint resource_to_unlock);
        void ChangeResourceDisplay(uint resource_to_unlock);
        void DestroyUnloksWithCoordinates(Vector3 draw_origin);
        void Delete(TerrainUnlockerInformation information);
    }
}