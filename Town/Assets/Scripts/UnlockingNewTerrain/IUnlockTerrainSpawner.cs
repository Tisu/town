﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public interface IUnlockTerrainSpawner
    {
        void SpawnTerrainUnlock(Vector3 offset_world, Vector3 offset_hex, uint resource_to_unlock);
    }
}