﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public sealed class SpawnedTerrainUnlockManager : ISpawnedTerrainUnlockManager
    {
        private readonly List<TerrainUnlockerInformation> m_resources_managers_in_terrain_unlock = new List<TerrainUnlockerInformation>();

        public void Add(TerrainUnlockerInformation manager, uint resource_to_unlock)
        {
            manager.m_resources_manager.ManageResourceChanges(resource_to_unlock, resource_to_unlock);
            m_resources_managers_in_terrain_unlock.Add(manager);
        }

        public void ChangeResourceDisplay(uint resource_to_unlock)
        {
            foreach(var manager in m_resources_managers_in_terrain_unlock)
            {
                manager.m_resources_manager.SetWood(resource_to_unlock);
                manager.m_resources_manager.SetStone(resource_to_unlock);
            }
        }

        public void Delete(TerrainUnlockerInformation information)
        {
            m_resources_managers_in_terrain_unlock.Remove(information);
        }
        public void DestroyUnloksWithCoordinates(Vector3 draw_origin)
        {
            m_resources_managers_in_terrain_unlock.FindAll(x => x.m_draw_origin == draw_origin).ForEach(x => Object.Destroy(x.gameObject));
        }
    }
}