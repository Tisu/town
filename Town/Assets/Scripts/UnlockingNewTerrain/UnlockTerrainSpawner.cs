﻿using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public sealed class UnlockTerrainSpawner : IUnlockTerrainSpawner
    {
        public float SPAWN_THRESHOLD = 2f;
        private readonly GameObject m_terrain_unlock_vertical;
        private readonly GameObject m_terrain_unlock_horizontal;
        private readonly SpawnGridMono m_grid_spawner;
        private readonly ISpawnedTerrainUnlockManager m_spawned_terrain_unlock_manager;
        public UnlockTerrainSpawner(GameObject terrain_unlock_horizontal, 
            GameObject terrain_unlock_vertical, 
            SpawnGridMono grid_spawner,
            ISpawnedTerrainUnlockManager spawned_terrain_unlock_manager)
        {
            m_terrain_unlock_horizontal = terrain_unlock_horizontal;
            m_terrain_unlock_vertical = terrain_unlock_vertical;
            m_grid_spawner = grid_spawner;
            m_spawned_terrain_unlock_manager = spawned_terrain_unlock_manager;
        }

        public void SpawnTerrainUnlock(Vector3 offset_world, Vector3 offset_hex, uint resource_to_unlock)
        {
            var height = m_grid_spawner.GetHeight() + 1;
            var width = m_grid_spawner.GetWidth() + 1;

            var spawn_point = new Vector3(0, height + SPAWN_THRESHOLD, 0) + offset_world;
            var hit = Physics2D.OverlapPoint(spawn_point);
            GameObject spawned;
            if(hit == null)
            {
                //top
                spawned = Object.Instantiate(m_terrain_unlock_vertical, spawn_point, Quaternion.identity) as GameObject;
                var information = spawned.AddComponent<TerrainUnlockerInformation>();
                information.m_draw_origin = new Vector3(0, m_grid_spawner.GetHeight() + 1, 0) + offset_hex;
                AddManager(spawned, resource_to_unlock, information);
            }
            spawn_point = new Vector3(0, -height - SPAWN_THRESHOLD, 0) + offset_world;
            hit = Physics2D.OverlapPoint(spawn_point);
            if(hit == null)
            {
                //bottom
                spawned = Object.Instantiate(m_terrain_unlock_vertical, spawn_point, Quaternion.identity) as GameObject;
                var information = spawned.AddComponent<TerrainUnlockerInformation>();
                information.m_draw_origin = new Vector3(0, -m_grid_spawner.GetHeight() - 1, 0) + offset_hex;
                AddManager(spawned, resource_to_unlock, information);
            }
            spawn_point = new Vector3(width + SPAWN_THRESHOLD, 0, 0) + offset_world;
            hit = Physics2D.OverlapPoint(spawn_point);
            if(hit == null)
            {
                //right
                spawned = Object.Instantiate(m_terrain_unlock_horizontal, spawn_point,
                        m_terrain_unlock_horizontal.transform.rotation) as GameObject;
                var information = spawned.AddComponent<TerrainUnlockerInformation>();
                information.m_draw_origin = new Vector3(m_grid_spawner.GetWidth() + 1, 0, 0) + offset_hex;
                AddManager(spawned, resource_to_unlock, information);
            }
            spawn_point = new Vector3(-width - SPAWN_THRESHOLD, 0, 0) + offset_world;
            hit = Physics2D.OverlapPoint(spawn_point);
            if(hit == null)
            {
                //left
                spawned =
                    Object.Instantiate(m_terrain_unlock_horizontal, spawn_point, Quaternion.Euler(new Vector3(0, 0, 90))) as
                        GameObject;
                var information = spawned.AddComponent<TerrainUnlockerInformation>();
                information.m_draw_origin = new Vector3(-m_grid_spawner.GetWidth() - 1, 0, 0) + offset_hex;
                AddManager(spawned, resource_to_unlock, information);
            }
        }

        private void AddManager(GameObject spawned, uint resource_to_unlock, TerrainUnlockerInformation information)
        {
            var ui_manager = spawned.GetComponent<UIResourcesManager>();
            information.m_resources_manager = ui_manager;
            m_spawned_terrain_unlock_manager.Add(information, resource_to_unlock);
        }
    }
}