﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.SoundManager
{
    public sealed class VolumeDisplayManager : MonoBehaviour
    {
        public const string VOLUME_FORMAT = "Volume: {0} %";
        [SerializeField]
        private Text m_volume_display_text;
        [SerializeField]
        private Slider m_slider;

        void Awake()
        {
            OnSliderChange();
        }
        public void OnSliderChange()
        {
            m_volume_display_text.text = string.Format(VOLUME_FORMAT, (int)(m_slider.value * 100));
        }
    }
}