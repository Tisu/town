﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Buildings;
using Assets.Scripts.SpriteExtender;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.SoundManager
{
    [RequireComponent(typeof(AudioSource))]
    public sealed class SoundManager : MonoBehaviour
    {
        [SerializeField]
        private AudioClip[] m_church_clips;
        [SerializeField]
        private AudioClip[] m_default_audio_clips;
        [SerializeField]
        private AudioClip[] m_market_clips;
        [SerializeField]
        private AudioClip[] m_cityhall_clips;
        [SerializeField]
        private AudioClip[] m_house_clips;
        [SerializeField]
        private AudioClip[] m_quarry_clips;
        [SerializeField]
        private AudioClip[] m_woodscutter_clips;
        public BuildingsRunner m_buildings_runner
        {
            get
            {
                return _buildings_runner;
            }
            set
            {
                if(_buildings_runner != null)
                {
                    return;
                }
                _buildings_runner = value;
                _buildings_runner.m_notify_about_building_run += ChangeBuildingMusic;
                _buildings_runner.m_notify_about_interrupted_execution += ChangeToDefaultMusic;
            }
        }

        private BuildingsRunner _buildings_runner;

        private readonly Dictionary<string, AudioClip[]> m_custom_audio_clips = new Dictionary<string, AudioClip[]>();
        private AudioSource m_audio_source;
        private float m_last_time_played;
        private string m_last_runner_played;
        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject);
            m_audio_source = GetComponent<AudioSource>();
            m_custom_audio_clips.Add(BuildingsNames.CHURCH, m_church_clips);
            m_custom_audio_clips.Add(BuildingsNames.MARKET, m_market_clips);
            m_custom_audio_clips.Add(BuildingsNames.CITY_HALL, m_cityhall_clips);
            m_custom_audio_clips.Add(BuildingsNames.HOUSE, m_house_clips);
            m_custom_audio_clips.Add(BuildingsNames.QUARRY, m_quarry_clips);
            m_custom_audio_clips.Add(BuildingsNames.WOODS_CUTTER, m_woodscutter_clips);
        }

        void Update()
        {
            if(m_audio_source.isPlaying)
            {
                return;
            }
            if(string.IsNullOrEmpty(m_last_runner_played))
            {
                ChangeToDefaultMusic();
                return;
            }
            ChangeEndedMusicToDifferentOne();
        }
        void OnDestroy()
        {
            if(m_buildings_runner == null)
            {
                return;
            }
            m_buildings_runner.m_notify_about_building_run -= ChangeBuildingMusic;
            m_buildings_runner.m_notify_about_interrupted_execution -= ChangeToDefaultMusic;
        }
        public void OnSliderValueChange(Slider slider)
        {
            m_audio_source.volume = slider.value;
        }
        public void OnSliderValueChange(float value)
        {
            m_audio_source.volume = value;
        }
        public void OnMuteClickChange(Toggle toggle)
        {
            m_audio_source.mute = toggle.isOn;
        }
        public void OnMuteClickChange(bool state)
        {
            m_audio_source.mute = state;
        }
        private void ChangeToDefaultMusic()
        {
            if(string.IsNullOrEmpty(m_last_runner_played))
            {
                return;
            }
            m_audio_source.clip = m_default_audio_clips.FirstOrDefault();
            m_audio_source.time = m_last_time_played;
            m_audio_source.Play();
            m_last_runner_played = string.Empty;
        }
        private void ChangeBuildingMusic(object sender, CustomBuildingsRunnerArg arg)
        {
            AudioClip[] audio_source;
            if(!m_custom_audio_clips.TryGetValue(arg.m_building_name, out audio_source))
            {
                return;
            }
            m_last_time_played = m_audio_source.time;
            m_audio_source.clip = audio_source.Random();
            m_audio_source.time = 0;
            m_audio_source.Play();
            m_last_runner_played = arg.m_building_name;
        }

        private void ChangeEndedMusicToDifferentOne()
        {
            AudioClip[] audio_source;
            if(!m_custom_audio_clips.TryGetValue(m_last_runner_played, out audio_source))
            {
                return;
            }
            m_audio_source.clip = audio_source.RandomAndIsNot(m_audio_source.clip);
            m_audio_source.Play();
            m_audio_source.time = 0;
        }
    }
}