﻿
using System;
using System.Collections.Generic;
using System.Linq;

public class FileGeneratorManager : FileGenerator
{
    public FileGeneratorManager(string a_file_name, eContentType a_content_type, bool a_append = false, string a_custom_content = null)
        : base(a_file_name, a_content_type, a_append, a_custom_content)
    {

    }
    public void MakeFileDistinct<T>(IEnumerable<T> a_container, Func<T, string> a_getter)
    {
        var distinct_container = a_container.Distinct(new EqualityComparer<T>(a_getter)).ToList();
        foreach (T item in distinct_container)
        {
            WriteLine(a_getter(item));
        }
        if (distinct_container.Count != a_container.Count())
        {
            Tisu.Logger.LogError("Detected identical members (skipped): " + (a_container.Count() - distinct_container.Count));
        }
        Dispose();
    }
    [Obsolete("Exists only for tests purposes")]
    protected IEnumerable<T> CustomDistinct<T>(IEnumerable<T> a_container, Func<T, string> a_getter)
    {
        HashSet<T> container_list = new HashSet<T>();
        int start_index = 1;
        foreach (var item in a_container)
        {
            bool unique = true;
            for (int i = start_index; i < a_container.Count(); i++)
            {
                if (a_getter(item) == a_getter(a_container.ElementAt(i)))
                {
                    unique = false;
                    break;
                }
            }
            if (unique)
            {
                container_list.Add(item);
            }
            start_index++;
        }
        return container_list;
    }
}

