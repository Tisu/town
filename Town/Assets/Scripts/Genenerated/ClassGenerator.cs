﻿public enum eProtectionLevel
{
    public_level,
    protected_level,
    private_level,
}
public enum eType
{
    int_type,
    float_type,
    string_type,
}
class ClassGenerator : FileGenerator
{
    const string MEMBER_HEADER = "m_";
    public ClassGenerator(string a_class_name) : base(a_class_name, eContentType.type_class)
    {

    }
    public void WriteMember(string a_member_name, eProtectionLevel a_protection_level, eType a_type)
    {
        string line = "";
        switch (a_protection_level)
        {
            case eProtectionLevel.public_level:
                line += "public ";
                break;
            case eProtectionLevel.protected_level:
                line += "protected ";
                break;
            case eProtectionLevel.private_level:
                line += "private ";
                break;
            default:
                break;
        }
        switch (a_type)
        {
            case eType.int_type:
                line += "int ";
                break;
            case eType.float_type:
                line += "float ";
                break;
            case eType.string_type:
                line += "string ";
                break;
            default:
                break;
        }
        line += MEMBER_HEADER + a_member_name;
        WriteLine(line);
    }
    public void WriteCustomMember(string a_custom_member)
    {
        WriteLine(a_custom_member);
    }
}

