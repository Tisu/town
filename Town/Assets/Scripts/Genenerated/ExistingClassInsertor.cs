﻿
using System;
using System.IO;

public class ExistingClassInsertor : IDisposable
{
    readonly StreamWriter m_stream_writer;
    string m_file_content;
    int m_position_to_insert;
    readonly string HEADER = "/*/n This file is auto inserted!! DO NOT CHANGE IT UNLESS YOU KNOW WHAT YOU ARE DOING /n*/";
    public ExistingClassInsertor(string a_file_path_with_extension, string a_start_point)
    {
        if (!File.Exists(a_file_path_with_extension))
        {
            return;
        }
        HEADER += "start point of insertion: " + a_start_point + "/n*/";
        m_file_content = File.ReadAllText(a_file_path_with_extension);
        WriteHeader(m_file_content);

        m_stream_writer = new StreamWriter(a_file_path_with_extension);
        PrepareFileStream(ref m_file_content, ref a_start_point);
    }

    private void WriteHeader(string m_file_content)
    {
        if (m_file_content.IndexOf("/*") != 0)
        {
            m_file_content = m_file_content.Insert(0, HEADER);
        }
    }

    public void Dispose()
    {
        if (m_stream_writer != null && m_stream_writer.BaseStream.CanWrite)
        {
            m_stream_writer.Write(m_file_content);
            m_stream_writer.Flush();
            m_stream_writer.Close();
        }
    }
    public void InsertNewLine(string a_new_line_to_insert)
    {
        if (m_position_to_insert <= 0)
        {
            Tisu.Logger.LogError("Wrong start point! ");
            return;
        }
        if (a_new_line_to_insert == null)
        {
            return;
        }

        m_file_content = m_file_content.Insert(m_position_to_insert, "\t\t" + a_new_line_to_insert + Environment.NewLine);
        m_position_to_insert += a_new_line_to_insert.Length + Environment.NewLine.Length + 2;
    }
    protected void PrepareFileStream(ref string a_content, ref string a_start_point)
    {
        m_position_to_insert = a_content.IndexOf(a_start_point) + a_start_point.Length;
        var delete_position = a_content.IndexOf("}", m_position_to_insert);
        if (delete_position > 0)
        {
            a_content = a_content.Remove(m_position_to_insert, delete_position - m_position_to_insert);            
        }
        InsertNewLine(Environment.NewLine);
    }

}

