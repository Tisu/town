﻿
using System;
using System.Collections.Generic;

public class EqualityComparer<T> : IEqualityComparer<T>
{
    readonly Func<T, string> m_getter;
    public EqualityComparer(Func<T,string> a_getter)
    {
        m_getter = a_getter;
    }
    public bool Equals(T x, T y)
    {
        return m_getter(x) == m_getter(y);
    }

    public int GetHashCode(T obj)
    {
        return obj.GetHashCode();
    }
}

