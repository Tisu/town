﻿using System;

namespace Assets.Scripts.Buildings
{
    public class CustomBuildingsRunnerArg : EventArgs
    {
        public readonly string m_building_name;

        public CustomBuildingsRunnerArg(string building_name)
        {
            m_building_name = building_name;
        }
    }
}