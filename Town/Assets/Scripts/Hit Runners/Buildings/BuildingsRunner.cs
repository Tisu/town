﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GridDisplay;
using Assets.Scripts.HitRunners;
using Commands;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Buildings
{
    public class BuildingsRunner : MonoBehaviour, IHitRunner
    {
        [SerializeField]
        private GameObject m_main_display;
        [SerializeField]
        private GameObject m_unlock_building_display;
        [SerializeField]
        private GameObject m_market_display;
        private readonly Dictionary<string, GameObject> m_building_name_to_display = new Dictionary<string, GameObject>();

        private GameObject m_current_display;
        public event EventHandler<CustomBuildingsRunnerArg> m_notify_about_building_run;
        public event Action m_notify_about_interrupted_execution;
        private IMarkRange m_mark_range;
        private MarkBuilding m_mark_building = new MarkBuilding();
        void Awake()
        {
            m_mark_range = new MarkRange(m_mark_building);
            m_building_name_to_display.Add(BuildingsNames.CITY_HALL, m_unlock_building_display);
            m_building_name_to_display.Add(BuildingsNames.MARKET, m_market_display);
            GameObject.Find("SoundManager").GetComponent<SoundManager.SoundManager>().m_buildings_runner = this;
        }
        public void RunActionClick(Collider2D hit, INotifyEndOfCommand end_of_command)
        {

            var grid_cell_info = hit.GetComponent<GridCellInfo>();
            if (grid_cell_info == null)
            {
                return;
            }
            var building_name = grid_cell_info.m_sprite_renderer.sprite.name;
            if(m_building_name_to_display.TryGetValue(building_name, out m_current_display))
            {
                m_main_display.SetActive(true);
                m_current_display = m_building_name_to_display[building_name];
                m_current_display.SetActive(true);
            }
            else
            {
                Debug.Log("Add custom window to building");
            }

            if(m_notify_about_building_run != null)
            {
                m_notify_about_building_run(this, new CustomBuildingsRunnerArg(building_name));
            }
            var building = BuildingsLoadedCache.GetBuildingBySpriteName(building_name);
            if(building == null)
            {
                Debug.Log("Not added to cache.");
                return;
            }
            m_mark_building.SetResourceDependecy(building, grid_cell_info.m_hex_grid);
            m_mark_range.MarkDistance(hit.gameObject, building.GetActionRange());
        }

        public void RunMousePositionHit(Collider2D hit)
        {
        }
        public Command GetCommand()
        {
            return new Command(CommandType.BuildingClick, CommandExecutionType.Periodic);
        }
        public void InterruptExecution()
        {
            if(m_current_display != null)
            {
                m_main_display.SetActive(false);
                m_current_display.SetActive(false);
                m_current_display = null;
            }
            if(m_notify_about_interrupted_execution != null)
            {
                m_notify_about_interrupted_execution();
            }
            m_mark_range.RemoveMarkedDistance();
        }
        public bool IsPeriodicSubscribeFor(string tag)
        {
            return false;
        }
        public string GetTagForWhichIsSubscribed()
        {
            return Tags.Tags.BUILDING;
        }
    }
}