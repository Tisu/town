﻿using System;
using Assets.Scripts.Inventory.Tap;
using UnityEngine;

namespace Assets.Scripts.HitRunners
{
    public sealed class RunnersSubscriber : MonoBehaviour
    {
        [SerializeField]
        private InputProvider m_input_provider;
        [SerializeField]
        private MonoBehaviour[] m_runners;
        void Awake()
        {
            if (m_runners == null)
            {
                throw new Exception("add runners!");
            }
            foreach(var runner in m_runners)
            {
                var hit_runner = runner as IHitRunner;
                if(hit_runner == null)
                {
                    continue;
                }
                m_input_provider.SubscribeRunner(hit_runner.GetTagForWhichIsSubscribed(), hit_runner);
            }
        }
    }
}