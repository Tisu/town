﻿using System;
using Assets.Scripts.GridDisplay;
using UnityEngine;

namespace Assets.Scripts.Inventory.Tap
{
    public sealed class MousePositionInventoryHitRunner : IMousePositionInventoryHitRunner
    {
        private readonly IMarkRange m_mark_range;
        private Vector3 m_lasted_marked_position;
        readonly SpriteGridChanger m_sprite_grid_changer = new SpriteGridChanger();
        private readonly MarkBuilding m_special_mark;
        public MousePositionInventoryHitRunner()
        {
            m_special_mark = new MarkBuilding();
            m_mark_range = new MarkRange(m_special_mark);
        }
        public void DisplayBuildingOnGrid(Collider2D hit_object, Func<IBuildingInformation> building_info_method)
        {
            if(m_lasted_marked_position == hit_object.gameObject.transform.position)
            {
                return;
            }

            RestoreLastChange();

            ChangeToNewPosition(hit_object, building_info_method);
            m_lasted_marked_position = hit_object.gameObject.transform.position;
        }

        private void ChangeToNewPosition(Collider2D hit_object, Func<IBuildingInformation> building_info_method)
        {
            var building = building_info_method();
            if(building == null)
            {
                return;
            }
            var cell_info = hit_object.gameObject.GetComponent<GridCellInfo>();

            if(cell_info == null)
            {
                Debug.LogError(hit_object.gameObject + "doesnt have gridcellinfo");
                return;
            }
            var hex_grid = cell_info.m_hex_grid;
            m_special_mark.SetResourceDependecy(building_info_method(), hex_grid);
            m_mark_range.MarkDistance(hex_grid, building.GetActionRange());
            m_sprite_grid_changer.ChangeSprite(hit_object.gameObject, building.GetSprite());
        }

        public void RestoreLastChange()
        {
            m_mark_range.RemoveMarkedDistance();
            m_sprite_grid_changer.RestoreLastSprite();
        }

        public void Clear()
        {
            RestoreLastChange();
            m_sprite_grid_changer.Clear();
            m_mark_range.RemoveMarkedDistance();
        }
    }
}