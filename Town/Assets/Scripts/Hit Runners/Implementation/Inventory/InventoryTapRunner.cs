﻿using Assets.Scripts.Buildings;
using Assets.Scripts.Buildings.BuilderOnMap;
using Assets.Scripts.UI_Managers;
using Commands;
using UnityEngine;

namespace Assets.Scripts.Inventory.Tap
{
    public sealed class InventoryTapRunner : MonoBehaviour, IHitRunner
    {
        [SerializeField]
        private UIInformationWindow m_information_window;
        [SerializeField]
        private GameObject m_progress_bar;
        [SerializeField]
        private BuildingResourceCache m_building_resource_cache;

        private readonly IMousePositionInventoryHitRunner m_mouse_position_inventory_hit_runner = new MousePositionInventoryHitRunner();
        private IBuildingsPreBuilder m_buildings_pre_builder;
        private IInventoryTap m_last_tap_target;

        void Awake()
        {
            m_buildings_pre_builder = new BuildingsPreBuilder(m_progress_bar, m_building_resource_cache);
        }
        public void RunActionClick(Collider2D hit, INotifyEndOfCommand notify_end_of_command)
        {
            if(hit.tag == Tags.Tags.INVENTORY)
            {
                ClickOnInventory(hit);
            }
            else if(IsPeriodicSubscribeFor(hit.tag))
            {
                ClickOnGrid(hit, notify_end_of_command);
            }
        }

        private void ClickOnGrid(Collider2D hit, INotifyEndOfCommand notify_end_of_command)
        {
            var grid_cell_info = hit.GetComponent<GridCellInfo>();
            if(grid_cell_info == null || m_last_tap_target == null)
            {
                return;
            }
            m_mouse_position_inventory_hit_runner.RestoreLastChange();
            m_buildings_pre_builder.PreBuild(hit.gameObject, m_last_tap_target.GetMountedSpriteName());
            EndCommandExecution(notify_end_of_command);
        }

        private void ClickOnInventory(Collider2D hit)
        {
            if(m_last_tap_target != null)
            {
                DisableLastTap();
            }
            var new_tap_target = hit.GetComponent<InventoryBackgroundTap>() as IInventoryTap;
            if(new_tap_target == null)
            {
                return;
            }
            if(new_tap_target == m_last_tap_target)
            {
                m_last_tap_target = null;
                return;
            }
            var building = GetBuildingInfo(new_tap_target);
            m_information_window.SetBuildingInformation(building);
            m_last_tap_target = new_tap_target;
            m_last_tap_target.OnTap();
        }

        private void DisableLastTap()
        {
            m_mouse_position_inventory_hit_runner.RestoreLastChange();
            m_last_tap_target.DisableLastTap();
        }

        private IBuildingInformation GetBuildingInfo(IInventoryTap tap)
        {
            var name = tap.GetMountedSpriteName();
            return BuildingsLoadedCache.GetBuildingBySpriteName(name);
        }
        private IBuildingInformation GetLastTapedBuildingInfo()
        {
            return GetBuildingInfo(m_last_tap_target);
        }
        private void EndCommandExecution(INotifyEndOfCommand notify_end_of_command)
        {
            InterruptExecution();
            m_information_window.DisableInformationWindow();
            notify_end_of_command.NotifyEndOfCommand();
            m_mouse_position_inventory_hit_runner.Clear();
        }

        public void RunMousePositionHit(Collider2D hit)
        {
            if(m_last_tap_target == null || hit.tag == Tags.Tags.BUILDING)
            {
                return;
            }
            m_mouse_position_inventory_hit_runner.DisplayBuildingOnGrid(hit, GetLastTapedBuildingInfo);
        }
        public Command GetCommand()
        {
            return new Command(CommandType.InventoryClick, CommandExecutionType.Periodic);
        }
        public void InterruptExecution()
        {
            m_mouse_position_inventory_hit_runner.Clear();
            m_information_window.DisableInformationWindow();
            m_last_tap_target.DisableLastTap();
            m_last_tap_target = null;
        }

        public bool IsPeriodicSubscribeFor(string tag)
        {
            return tag == Tags.Tags.HEXGRID || tag == Tags.Tags.RESOURCE;
        }

        public string GetTagForWhichIsSubscribed()
        {
            return Tags.Tags.INVENTORY;
        }
    }
}