﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Inventory.Tap
{
    public sealed class SpriteGridChanger
    {
        private Sprite m_last_sprite;
        private SpriteRenderer m_last_renderer;

        public void ChangeSprite(GameObject game_object, Sprite new_sprite)
        {
            m_last_renderer = game_object.GetComponent<SpriteRenderer>();
            m_last_sprite = m_last_renderer.sprite;
            m_last_renderer.sprite = new_sprite;
        }
        public void RestoreLastSprite()
        {
            if(m_last_renderer == null || m_last_sprite == null)
            {
                return;
            }
            m_last_renderer.sprite = m_last_sprite;
        }

        public void Clear()
        {
            m_last_renderer = null;
            m_last_sprite = null;
        }
    }
}