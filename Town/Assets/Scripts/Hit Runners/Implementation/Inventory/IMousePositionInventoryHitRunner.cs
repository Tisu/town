﻿using System;
using UnityEngine;

namespace Assets.Scripts.Inventory.Tap
{
    public interface IMousePositionInventoryHitRunner
    {
        void Clear();
        void DisplayBuildingOnGrid(Collider2D hit_object, Func<IBuildingInformation> building_info_method);
        void RestoreLastChange();
    }
}