﻿using Commands;
using UnityEngine;

namespace Assets.Scripts.UnlockingNewTerrain
{
    public sealed class TerrainUnlockerRunner : MonoBehaviour, IHitRunner
    {
        private const int RESOURCE_TO_UNLOCK_CHANGE = 3;
        [SerializeField]
        private uint m_resource_to_unlock_value;
        [SerializeField]
        private SpawnGridMono m_grid_spawner;

        [SerializeField]
        private GameObject m_terrain_unlock_vertical;
        [SerializeField]
        private GameObject m_terrain_unlock_horizontal;

        [SerializeField]
        private MainCamera m_main_camera;

        private uint m_resource_to_unlock
        {
            get { return m_resource_to_unlock_value; }
            set
            {
                m_resource_to_unlock_value = value;
                m_spawned_terrain_unlock_manager.ChangeResourceDisplay(value);
            }
        }
        private ICameraMaxPositionAdjuster m_camera_max_position_adjuster;
        private IUnlockTerrainSpawner m_unlock_terrain_spawner;
        private ISpawnedTerrainUnlockManager m_spawned_terrain_unlock_manager;
        void Start()
        {
            //hack GridCellInfo have to add grids previously generated in editor before adding new grids
            GridGameController.m_instance.ToString();
            m_spawned_terrain_unlock_manager = new SpawnedTerrainUnlockManager();
            m_camera_max_position_adjuster = new CameraMaxPositionAdjuster(m_main_camera, m_grid_spawner);
            m_unlock_terrain_spawner = new UnlockTerrainSpawner(m_terrain_unlock_horizontal, m_terrain_unlock_vertical, m_grid_spawner, m_spawned_terrain_unlock_manager);

            m_unlock_terrain_spawner.SpawnTerrainUnlock(Vector3.zero, Vector3.zero, m_resource_to_unlock);

            m_spawned_terrain_unlock_manager.ChangeResourceDisplay(m_resource_to_unlock);
        }

        public void RunActionClick(Collider2D hit, INotifyEndOfCommand end_of_command)
        {
            if(!ResourceChecker.CheckIfEnoughtResources(m_resource_to_unlock, m_resource_to_unlock))
            {
                return;
            }
            ResourcesManager.m_instance.m_stone -= m_resource_to_unlock;
            ResourcesManager.m_instance.m_wood -= m_resource_to_unlock;

            m_resource_to_unlock += RESOURCE_TO_UNLOCK_CHANGE;

            var terrain_information = hit.GetComponent<TerrainUnlockerInformation>();
            m_spawned_terrain_unlock_manager.Delete(terrain_information);
            Destroy(hit.gameObject);

            m_grid_spawner.Generate(terrain_information.m_draw_origin);

            var world_position_of_origin = GridFrameworkPositionProvider.m_instance.GridToWorld(terrain_information.m_draw_origin);

            m_camera_max_position_adjuster.ChangeCameraMaxPosition(world_position_of_origin);
            m_unlock_terrain_spawner.SpawnTerrainUnlock(world_position_of_origin, terrain_information.m_draw_origin, m_resource_to_unlock);
            m_spawned_terrain_unlock_manager.DestroyUnloksWithCoordinates(terrain_information.m_draw_origin);

            end_of_command.NotifyEndOfCommand();
        }
        public void RunMousePositionHit(Collider2D hit)
        {
        }

        public Command GetCommand()
        {
            return new Command(CommandType.UnlockTerrain, CommandExecutionType.PeriodicCanBeInterrupted);
        }
        public void InterruptExecution()
        {
        }
        public bool IsPeriodicSubscribeFor(string tag)
        {
            return false;
        }
        public string GetTagForWhichIsSubscribed()
        {
            return Tags.Tags.UNLOCK_TERRAIN;
        }
    }
}