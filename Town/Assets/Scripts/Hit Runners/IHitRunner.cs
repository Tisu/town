﻿using Commands;
using UnityEngine;

public interface IHitRunner
{
    void RunActionClick(Collider2D hit, INotifyEndOfCommand end_of_command);
    void RunMousePositionHit(Collider2D hit);
    Command GetCommand();
    void InterruptExecution();
    bool IsPeriodicSubscribeFor(string tag);
    string GetTagForWhichIsSubscribed();
}