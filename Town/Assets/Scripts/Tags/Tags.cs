﻿using System;

namespace Assets.Scripts.Tags
{
    public struct Tags
    {
        public const string HEXGRID = "HexGrid";
        public const string INVENTORY = "Inventory";
        public const string BUILDING = "Building";
        public const string RESOURCE = "Resource";
        public const string CANNOT_CLICK_OVER = "CannotClickOver";
        public const string UNLOCK_TERRAIN = "UnlockTerrain";
    }
}