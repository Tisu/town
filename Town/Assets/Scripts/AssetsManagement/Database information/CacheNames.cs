﻿namespace Assets.Scripts.AssetsManagement
{
    public static class CacheNames
    {
        public const string BUILDINGS = "Building";
        public const string TECHNOLOGY = "Technology";
        public const string SPRITES = "Sprites";
    }
}