﻿
public static class DatabaseInformation
{
    const string SEPARATOR = @"/";
    public const string RESOURCE_FOLDER = "Resources";
    public const string FIRST_FOLDER = @"Assets";
    public const string CUSTOM_DATABASE_FOLDER = @"CustomDatabase";

    public static readonly string FOLDER_NAME = RESOURCE_FOLDER + SEPARATOR + CUSTOM_DATABASE_FOLDER;

    public static readonly string DATABASE_PATH_WITHOUT_SEPARATOR = FIRST_FOLDER + SEPARATOR + FOLDER_NAME;
    public static readonly string RESOURCES_PATH_WITHOUT_SEPARATOR = FIRST_FOLDER + SEPARATOR + RESOURCE_FOLDER;

    public static readonly string DATABASE_PATH = FIRST_FOLDER + SEPARATOR + FOLDER_NAME + SEPARATOR;
}

