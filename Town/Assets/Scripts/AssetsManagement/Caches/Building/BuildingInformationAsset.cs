﻿using System;
using Assets.Scripts.AssetsManagement.Caches.Building;
using UnityEngine;

[Serializable]
public class BuildingInformationAsset : ScriptableObject, ICacheName, IBuildingInformation
{
    [SerializeField]
    public string m_name;
    [SerializeField]
    public int m_wood_cost;
    [SerializeField]
    public int m_stone_cost;
    [SerializeField]
    public Sprite m_sprite;
    [SerializeField]
    public string m_description;
    [SerializeField]
    public Technology m_technology_to_unlock;
    [SerializeField]
    public int m_action_range;
    [SerializeField]
    public string m_resource_dependency;
    [SerializeField]
    public float m_building_time;

    [SerializeField]
    public int m_selected_technology_index;

    string ICacheName.m_name
    {
        get
        {
            return m_name;
        }

        set
        {
            m_name = value;
        }
    }

    public string GetSpriteName()
    {
        if(m_sprite == null)
        {
            return null;
        }
        return m_sprite.name;
    }
    public uint GetStoneCost()
    {
        return (uint)m_stone_cost;
    }
    public uint GetWoodCost()
    {
        return (uint)m_wood_cost;
    }

    public int GetActionRange()
    {
        return m_action_range;
    }
    public string GetResourceDependency()
    {
        return m_resource_dependency;
    }

    public float GetBuildingTime()
    {
        return m_building_time;
    }

    public string GetName()
    {
        return m_name;
    }
    public Sprite GetSprite()
    {
        return m_sprite;
    }

    public string GetDescription()
    {
        return m_description;
    }
    public  BuildingInformation Clone()
    {
        return new BuildingInformation()
        {
            m_wood_cost = this.m_wood_cost,
            m_stone_cost = this.m_stone_cost,
            m_sprite = this.m_sprite,
            m_description = this.m_description,
            m_action_range = this.m_action_range,
            m_technology_to_unlock = this.m_technology_to_unlock,
            m_resource_dependency = this.m_resource_dependency,
            m_name = this.m_name,
            m_building_time = this.m_building_time
        };
    }
}

