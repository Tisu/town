﻿using System;
using System.Linq;
using System.Reflection;
using Assets.Scripts.Buildings;

namespace Assets.Scripts.AssetsManagement.Caches.Building
{
    public class ObjectType
    {
        public const char TAG_DEPENDENCY_MARKER = '#';
        public const char BUILDING_NAME_DEPENDENCY_MARKER = '$';

        public const string WOOD = "wood";
        public const string STONE = "stone";
        public const string OBSTACLE = "obstacle";
        public const string UNDEFINED = "undefined";

        public static readonly string[] RESOURCE =
        {
           WOOD,STONE,
        };
        public static readonly string[] TAG_DEPENDENCY;
        public static readonly string[] SPRITE_NAME_DEPENDENCY = { UNDEFINED, WOOD, STONE, OBSTACLE, };

        public static readonly string[] ARRAY;
        public static readonly string[] BUILDING_NAME_DEPENDENCY;

        static ObjectType()
        {
            Type type = new Tags.Tags().GetType();
            var fields = type.GetFields();
            TAG_DEPENDENCY = new string[fields.Length];
            for(int i = 0; i < fields.Length; i++)
            {
                TAG_DEPENDENCY[i] = TAG_DEPENDENCY_MARKER + (string)fields[i].GetRawConstantValue();
            }

            type = new BuildingsNames().GetType();
            fields = type.GetFields();
            BUILDING_NAME_DEPENDENCY = new string[fields.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                BUILDING_NAME_DEPENDENCY[i] = BUILDING_NAME_DEPENDENCY_MARKER + (string)fields[i].GetRawConstantValue();
            }
            ARRAY = SPRITE_NAME_DEPENDENCY.Concat(TAG_DEPENDENCY).Concat(BUILDING_NAME_DEPENDENCY).ToArray();
        }
    }
}