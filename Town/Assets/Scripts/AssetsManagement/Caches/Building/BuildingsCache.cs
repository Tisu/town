﻿[System.Serializable]
public class BuildingsCache : CacheBase<BuildingInformationAsset>
{
    public override void OnDestroy()
    {
        GenerateEnumFile();
        InsertToBuildingFactory();
    }

    private void InsertToBuildingFactory()
    {
        //todo uncomment this
        //var insert_to_factory = new InsertIntoBuildingDictionary();
        //insert_to_factory.Insert(m_cache);
    }

    private void GenerateEnumFile()
    {
       //var enum_generator = new FileGeneratorManager("BuildingsType", eContentType.type_enum);
       // enum_generator.MakeFileDistinct(m_cache, x => x.m_name);
    }
}


