﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class SpriteCache : ScriptableObject, ISpriteCache
{
    [SerializeField]
    public List<SpriteSingleCache> m_sprite_cache;
    public void OnEnable()
    {
        if (m_sprite_cache == null)
        {
            m_sprite_cache = new List<SpriteSingleCache>();
        }
    }
    public void OnGUI()
    {
        foreach (var instance in m_sprite_cache)
        {
            instance.OnGUI();
        }
    }
    public void CreateNewSprite()
    {
#if UNITY_EDITOR
        SpriteSingleCache new_sprite = CreateInstance<SpriteSingleCache>();
        UnityEditor.AssetDatabase.AddObjectToAsset(new_sprite, this);      
        new_sprite.OnGUI();
        m_sprite_cache.Add(new_sprite);
#endif
    }
    public void Sort()
    {
        m_sprite_cache.Sort((x, y) => -y.m_height.CompareTo(x.m_height));
        var last_sprite = m_sprite_cache.LastOrDefault();
        if (last_sprite != null)
        {
            last_sprite.SetHeightToMax();
        }
    }

    public void DeleteLastRow()
    {
        m_sprite_cache.RemoveAt(m_sprite_cache.Count - 1);
    }
}

