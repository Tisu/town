﻿
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class SpriteSingleCache : ScriptableObject
{
    [SerializeField]
    public float m_height;
    [SerializeField]
    public Sprite m_sprite; 

    public void OnGUI()
    {
#if UNITY_EDITOR
        
        EditorGUILayout.BeginHorizontal();
        m_height = EditorGUILayout.Slider(m_height, 0, 1f);
        m_sprite = EditorGUILayout.ObjectField(m_sprite, typeof(Sprite), true) as Sprite;
        EditorGUILayout.EndHorizontal();
#endif
    }
    public void SetHeightToMax()
    {
        m_height = 1;
    }
}

