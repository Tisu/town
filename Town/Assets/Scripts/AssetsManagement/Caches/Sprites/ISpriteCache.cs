﻿public interface ISpriteCache
{
    void CreateNewSprite();
    void DeleteLastRow();
    void OnEnable();
    void OnGUI();
    void Sort();
}