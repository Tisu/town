﻿using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[System.Serializable]
public abstract class CacheBase<T> : ScriptableObject, IWindowCache where T : ScriptableObject, ICacheName
{
    [SerializeField]
    public List<T> m_cache;

    public void AddNewToCache()
    {
        
#if UNITY_EDITOR
        var new_item = CreateInstance<T>();
        new_item.m_name = "New record";
        m_cache.Add(new_item);

        UnityEditor.AssetDatabase.AddObjectToAsset(new_item, this);
#endif
    }
    public bool OnGui(ref T a_current_technology)
    {
        bool button_clicked = false;

        foreach(var cache_item in m_cache)
        {
            if(cache_item == a_current_technology)
            {
                GUI.backgroundColor = Color.gray;
            }
            else
            {
                GUI.backgroundColor = Color.white;
            }
            if(GUILayout.Button(cache_item.m_name))
            {
                a_current_technology = cache_item;
                button_clicked = true;
            }
        }
        GUI.backgroundColor = Color.white;
        return button_clicked;
    }
    public void OnEnable()
    {
        if(m_cache == null)
        {
            m_cache = new List<T>();
        }
    }
    public void DeleteCurrent(ref T a_current_selected)
    {
        if(a_current_selected == null)
        {
            return;
        }
        var position = m_cache.IndexOf(a_current_selected);

        if(position >= 0 && m_cache.Count > 0)
        {
            m_cache.RemoveAt(position);
        }
        else
        {
            a_current_selected = null;
            return;
        }
        position = m_cache.Count > position ? position : position - 1;

        if(position >= 0 && m_cache.Count > 0)
        {
            a_current_selected = m_cache.ElementAt(position);
        }
    }
    public abstract void OnDestroy();
}


