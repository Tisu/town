﻿
using System;
using UnityEngine;

[Serializable]
public class Technology : ScriptableObject, ICacheName
{
    [SerializeField]
    public string m_name;
    [SerializeField]
    public sResource m_resource;
    [SerializeField]
    public Technology m_previous_technology;
    [SerializeField]
    public Technology m_next_technology;
    [SerializeField]
    public int m_next_technology_index;
    [SerializeField]
    public int m_previous_technology_index;

    string ICacheName.m_name
    {
        get
        {
            return m_name;
        }

        set
        {
            m_name = value;
        }
    }
    public override string ToString()
    {
        return m_name;
    }
}

