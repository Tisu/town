﻿
using System;

[Serializable]
public class TechnologyCache : CacheBase<Technology>
{    
    public override void OnDestroy()
    {
        var enum_generator = new FileGeneratorManager("Technology", eContentType.type_enum);
        enum_generator.MakeFileDistinct(m_cache, x => x.m_name);
    }
}

