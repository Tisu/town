﻿using UnityEngine;

namespace Assets.Scripts.AssetsManagement
{
    public static class LoadCache
    {
        public static BuildingsCache LoadBuildings()
        {
            return Resources.Load<BuildingsCache>(MakePath(CacheNames.BUILDINGS));
        }

        public static TechnologyCache LoadTechnology()
        {
            return Resources.Load<TechnologyCache>(MakePath(CacheNames.TECHNOLOGY));
        }
        public static SpriteCache LoadSprite()
        {
            return Resources.Load<SpriteCache>(MakePath(CacheNames.SPRITES));
        }
        private static string MakePath(string file_name)
        {
            return DatabaseInformation.CUSTOM_DATABASE_FOLDER + @"/" + file_name + "Cache";
        }
    }
}