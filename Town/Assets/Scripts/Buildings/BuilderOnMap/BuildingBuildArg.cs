﻿using System;

namespace Assets.Scripts.Buildings
{
    public class BuildingBuildArg : EventArgs
    {
        public readonly GridCellInfo m_grid_cell_info;
        public BuildingBuildArg(GridCellInfo grid_cell_info)
        {
            m_grid_cell_info = grid_cell_info;
        }
    }
}