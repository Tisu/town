﻿using Assets.Scripts.Inventory.Tap;
using UnityEngine;

namespace Assets.Scripts.Buildings.BuilderOnMap
{
    public sealed class BuildingsPreBuilder : IBuildingsPreBuilder
    {
        private const string BUILDING_TEXT = "Building...";
        private readonly GameObject m_progress_bar;
        private readonly IBuildingResourceCache m_resource_cache;
        public BuildingsPreBuilder(GameObject progress_bar, IBuildingResourceCache resource_cache)
        {
            m_progress_bar = progress_bar;
            m_resource_cache = resource_cache;
        }

        public void PreBuild(GameObject clicked_grid, string new_sprite_naem)
        {
            IBuildingInformation building_information =
                BuildingsLoadedCache.GetBuildingBySpriteName(new_sprite_naem);
            if(building_information == null)
            {
                Debug.Log("Sprite not added to cache!");
                return;
            }

            if(!ResourcesManager.m_instance.SubtractBuildingCost(building_information))
            {
                return;
            }
            SpawnProgressBar(clicked_grid, building_information);

            clicked_grid.tag = Tags.Tags.BUILDING;
        }

        private void SpawnProgressBar(GameObject clicked_grid, IBuildingInformation building_information)
        {
            var progress_bar = Object.Instantiate(m_progress_bar, clicked_grid.transform.position, Quaternion.identity) as GameObject;
            if(progress_bar == null)
            {
                return;
            }
            var progress_bar_script = progress_bar.GetComponent<ProgressBar.ProgressBar>();
            progress_bar_script.m_grid_cell_traget = clicked_grid.GetComponent<GridCellInfo>();
            progress_bar_script.SetText(BUILDING_TEXT);
            progress_bar_script.m_progress_time = building_information.GetBuildingTime();
            var builder = new BuildingBuilderOnGrid(building_information, clicked_grid, m_resource_cache);
            progress_bar_script.m_on_finished = builder.BuildBuilding;
        }
    }
}