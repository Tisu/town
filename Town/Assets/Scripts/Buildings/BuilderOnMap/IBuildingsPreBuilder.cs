﻿using UnityEngine;

namespace Assets.Scripts.Buildings.BuilderOnMap
{
    public interface IBuildingsPreBuilder
    {
        void PreBuild(GameObject clicked_grid, string new_sprite_naem);
    }
}