﻿using System;
using Assets.Scripts.ProgressBar;
using UnityEngine;

namespace Assets.Scripts.Buildings
{
    public sealed class BuildingBuilderOnGrid
    {
        public static EventHandler<BuildingBuildArg> m_on_building_build;
        private readonly GameObject m_building_grid;
        private readonly IBuildingInformation m_building_information;
        private readonly IBuildingWorkerBuilder m_worker_builder;
        public BuildingBuilderOnGrid(IBuildingInformation building_information, GameObject building_grid, IBuildingResourceCache resource_cache)
        {
            m_building_information = building_information;
            m_building_grid = building_grid;
            m_worker_builder = new BuildingWorkerBuilder(resource_cache);
        }

        public void BuildBuilding()
        {
            m_worker_builder.AddBuildingWorker(m_building_grid, m_building_information);
            var sprite = m_building_information.GetSprite();
            m_building_grid.GetComponent<SpriteRenderer>().sprite = sprite;
            if(m_on_building_build != null)
            {
                m_on_building_build(this, new BuildingBuildArg(m_building_grid.GetComponent<GridCellInfo>()));
            }
        }
    }
}