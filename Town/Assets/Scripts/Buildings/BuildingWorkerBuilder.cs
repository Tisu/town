﻿using System;
using Assets.Scripts.Buildings.Work;
using Assets.Scripts.Buildings.Workers.Church;
using Assets.Scripts.Buildings.Workers.Farm;
using Assets.Scripts.Buildings.Workers.Field;
using Assets.Scripts.Buildings.Workers.House;
using Assets.Scripts.Buildings.Workers.Quarry;
using Assets.Scripts.SpriteExtender;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Buildings
{
    public sealed class BuildingWorkerBuilder : IBuildingWorkerBuilder
    {
        private IBuildingResourceCache m_resource_cache;
        public BuildingWorkerBuilder(IBuildingResourceCache resource_cache)
        {
            m_resource_cache = resource_cache;
        }

        public void AddBuildingWorker(GameObject grid, IBuildingInformation building)
        {
            switch(building.GetName())
            {
                case BuildingsNames.WOODS_CUTTER:
                    {
                        WoodsCutterWorker(grid, building);
                        break;
                    }
                case BuildingsNames.QUARRY:
                    {
                        QuarryWorker(grid);
                        break;
                    }
                case BuildingsNames.CHURCH:
                    {
                        ChurchWorker(grid, building);
                        break;
                    }
                case BuildingsNames.HOUSE:
                    {
                        grid.AddComponent<HouseWorker>();
                        break;
                    }
                case BuildingsNames.FARM:
                    {
                        grid.AddComponent<FarmWorker>();
                        break;
                    }
                case BuildingsNames.FIELD:
                    {
                        grid.AddComponent<FieldWorker>();
                        break;
                    }
                case BuildingsNames.CITY_HALL:
                case BuildingsNames.MARKET:
                case BuildingsNames.ARCHWAY:
                case BuildingsNames.TOWER:
                case BuildingsNames.RUINS:
                    Debug.Log("skipped adding worker: " + building.GetName());
                    break;
                default:
                    throw new Exception("Not added to architect : " + building.GetName());
            }
        }

        private void WoodsCutterWorker(GameObject grid, IBuildingInformation building)
        {
            var worker = grid.AddComponent<WoodCutterWorker>();
            worker.SetCache(m_resource_cache, building);
        }
        private void QuarryWorker(GameObject grid)
        {
            if(grid.GetComponent<SpriteRenderer>().sprite.IsStone())
            {
                var worker = grid.AddComponent<QuarryWorker>();
                worker.m_progress_bar = m_resource_cache.GetProgressBar();
                worker.StartWork();
            }
            else
            {
                var display_message = Object.Instantiate(m_resource_cache.GetDisplayMessageObect(), grid.transform.position,
                    Quaternion.identity) as GameObject;
                var text = display_message.GetComponentInChildren<Text>();
                text.text = "Out of " + Environment.NewLine + " resource";
            }
        }
        private void ChurchWorker(GameObject grid, IBuildingInformation building)
        {
            var worker = grid.AddComponent<ChurchWorker>();
            worker.SetCache(building);
        }
    }
}