﻿using Assets.Scripts.Commons;
using UnityEngine;

namespace Assets.Scripts.Buildings.Workers.Farm
{
    public sealed class FarmWorker : MonoBehaviour
    {
        public const int PRODUCTION_TIME = 30;
        private IUpdatePeriodExecutor m_update_period_executor;
        private int m_food_production = 2;
        void Awake()
        {
            m_update_period_executor = new UpdatePeriodExecutor(PRODUCTION_TIME, ProductFood, this);
        }
        void FixedUpdate()
        {
            m_update_period_executor.Update();
        }
        private void ProductFood()
        {
            ResourcesManager.m_instance.m_food += m_food_production;
        }
        public void IncrementFoodProduction()
        {
            ++m_food_production;
        }

        public void DecrementFoodProduction()
        {
            --m_food_production;
        }
    }
}