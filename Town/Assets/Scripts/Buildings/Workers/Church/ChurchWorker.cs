﻿using Assets.Scripts.Buildings.Work;
using Assets.Scripts.Buildings.Workers.Quarry;
using Assets.Scripts.GridFunctionProviders;
using UnityEngine;

namespace Assets.Scripts.Buildings.Workers.Church
{
    public sealed class ChurchWorker : MonoBehaviour
    {
        private const int RESOURCE_PRODUCTION_BUFF = 2;
        private int m_action_range;

        public void SetCache(IBuildingInformation building)
        {
            m_action_range = building.GetActionRange();
            BuildingBuilderOnGrid.m_on_building_build += OnBuildingBuilt;
        }
        private void OnBuildingBuilt(object sender, BuildingBuildArg e)
        {
            BuffBuilding(e.m_grid_cell_info);
        }
        public void BuffExistingBuildings()
        {
            var hex = GetComponent<GridCellInfo>().m_hex_grid;
            hex.ExecuteActionOnGridsInRange(m_action_range, BuffBuilding);
        }
        private void BuffBuilding(GridCellInfo obj)
        {
            switch(obj.m_sprite_renderer.sprite.name)
            {
                case BuildingsNames.QUARRY:
                    {
                        obj.GetComponent<QuarryWorker>().m_stone_production += RESOURCE_PRODUCTION_BUFF;
                        break;
                    }
                case BuildingsNames.WOODS_CUTTER:
                    {
                        obj.GetComponent<WoodCutterWorker>().m_wood_production += RESOURCE_PRODUCTION_BUFF;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        void OnDestroy()
        {
            var hex = GetComponent<GridCellInfo>().m_hex_grid;
            hex.ExecuteActionOnGridsInRange(m_action_range, DeBuffBuilding);
        }

        private void DeBuffBuilding(GridCellInfo obj)
        {
            switch(obj.m_sprite_renderer.sprite.name)
            {
                case BuildingsNames.QUARRY:
                    {
                        obj.GetComponent<QuarryWorker>().m_stone_production -= RESOURCE_PRODUCTION_BUFF;
                        break;
                    }
                case BuildingsNames.WOODS_CUTTER:
                    {
                        obj.GetComponent<WoodCutterWorker>().m_wood_production -= RESOURCE_PRODUCTION_BUFF;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }
}