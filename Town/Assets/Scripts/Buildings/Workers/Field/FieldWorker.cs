﻿using Assets.Scripts.Buildings.Workers.Farm;
using Assets.Scripts.GridFunctionProviders;
using UnityEngine;

namespace Assets.Scripts.Buildings.Workers.Field
{
    public sealed class FieldWorker : MonoBehaviour
    {
        private const int MAX_FIELD_PER_FARM = 1;
        private IBuildingInformation _building_information;
        public int m_farm_workers_added = 0;
        public IBuildingInformation m_building_information
        {
            get { return _building_information; }
            set
            {
                _building_information = value;
                var grid = GetComponent<GridCellInfo>();
                grid.m_hex_grid.ExecuteActionOnGridsInRange(m_building_information.GetActionRange(),MarkFarm);
            }
        }

        private void MarkFarm(GridCellInfo obj)
        {
            if (m_farm_workers_added >= MAX_FIELD_PER_FARM)
            {
                return;
            }
            var farm_worker = obj.GetComponent<FarmWorker>();
            if (farm_worker == null)
            {
                return;
            }
            farm_worker.IncrementFoodProduction();
            ++m_farm_workers_added;
        }

        void OnDestroy()
        {
            var grid = GetComponent<GridCellInfo>();
            grid.m_hex_grid.ExecuteActionOnGridsInRange(m_building_information.GetActionRange(), RemoveFieldFromFarm);
        }

        private void RemoveFieldFromFarm(GridCellInfo obj)
        {
            var farm_worker = obj.GetComponent<FarmWorker>();
            if(farm_worker == null)
            {
                return;
            }
            farm_worker.DecrementFoodProduction();
        }
    }
}