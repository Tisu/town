﻿using Assets.Scripts.Commons;
using UnityEngine;

namespace Assets.Scripts.Buildings.Work
{
    public sealed class TreeState : MonoBehaviour
    {
        private readonly int[] DEFAULT_DURATION = { 1, 2, 5 };
        public int m_harvested { get; set; }
        public SpriteRenderer m_sprite_renderer { get; private set; }
        void Awake()
        {
            m_sprite_renderer = GetComponent<SpriteRenderer>();
            gameObject.tag = Tags.Tags.RESOURCE;
        }
        public bool IsNotHarvestable()
        {
            return m_harvested >= DEFAULT_DURATION[SpriteNumber.GetNumber(m_sprite_renderer.sprite.name) - 1];
        }

        void OnDestroy()
        {
            gameObject.tag = Tags.Tags.HEXGRID;
        }
    }
}