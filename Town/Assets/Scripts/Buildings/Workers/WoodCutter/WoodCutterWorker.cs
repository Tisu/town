﻿using System;
using Assets.Scripts.Commons;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Buildings.Work
{
    public sealed class WoodCutterWorker : MonoBehaviour
    {
        private const float WORKER_RESTING_TIME = 25f;
        private const int PRODUCTION = 1;
        public uint m_wood_production { get; set; }

        private GameObject m_progress_bar { get; set; }
        private Sprite[] m_wood_sprites { get; set; }
        private Sprite[] m_tiles { get; set; }

        private IUpdatePeriodExecutor m_update_executor;
        private WoodCutterJobChooser m_wood_cutter_job_chooser;
        void Awake()
        {
            m_update_executor = new UpdatePeriodExecutor(WORKER_RESTING_TIME, ChooseJob, this);
            m_wood_production = PRODUCTION;
        }
        private void InstantiateProgressBar(string text, GridCellInfo grid_info, Action on_destroy_action)
        {
            var progress_bar = Instantiate(m_progress_bar, grid_info.transform.position, Quaternion.identity) as GameObject;
            var script = progress_bar.GetComponent<ProgressBar.ProgressBar>();
            script.m_grid_cell_traget = grid_info;
            script.SetProgressBarColor(Color.green);
            script.SetText(text);
            script.m_progress_time = 2f;
            script.m_on_finished = on_destroy_action;
        }
        void FixedUpdate()
        {
            m_update_executor.Update();
        }
        public void SetCache(IBuildingResourceCache resource_cache, IBuildingInformation building)
        {
            m_progress_bar = resource_cache.GetProgressBar();
            m_wood_sprites = resource_cache.GetWoodSprites();
            m_tiles = resource_cache.GetTilesSprites();
            m_wood_cutter_job_chooser = new WoodCutterJobChooser(building, GetComponent<GridCellInfo>().m_hex_grid, m_wood_sprites.Length);
        }
        public void ChooseJob()
        {
            GridCellInfo target;
            var job = m_wood_cutter_job_chooser.ChooseJob(out target);
            switch(job)
            {
                case WoodCutterJob.plant_tree:
                    InstantiateProgressBar("Planting...", target, () =>
                    {
                        target.m_sprite_renderer.sprite = m_wood_sprites[0];
                        target.gameObject.AddComponent<TreeState>();
                    });
                    break;
                case WoodCutterJob.grow_tree:
                    InstantiateProgressBar("Nurturing...", target, delegate
                    {
                        GrowTree(target);
                    }
                );
                    break;
                case WoodCutterJob.cut_tree:
                    InstantiateProgressBar("Cutting...", target, delegate
                    {
                        HarvestTree(target);
                    }
                  );
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void HarvestTree(GridCellInfo target)
        {
            var tree_info = target.GetComponent<TreeState>() ?? target.gameObject.AddComponent<TreeState>();
            tree_info.m_harvested++;
            ResourcesManager.m_instance.m_wood += m_wood_production;
            if(tree_info.IsNotHarvestable())
            {
                ChangeTreeSprite(target, tree_info);
            }
        }

        private void GrowTree(GridCellInfo target)
        {
            var tree_info = target.GetComponent<TreeState>() ?? target.gameObject.AddComponent<TreeState>();
            var number = SpriteNumber.GetNumber(tree_info.m_sprite_renderer.sprite.name);

            if(number >= m_wood_sprites.Length - 1)
            {
                Debug.Log("Somthing is wrong");
            }
            tree_info.m_sprite_renderer.sprite = m_wood_sprites[++number];
        }


        private void ChangeTreeSprite(GridCellInfo tree, TreeState tree_info)
        {
            var number = SpriteNumber.GetNumber(tree);

            if(number > 0)
            {
                tree.m_sprite_renderer.sprite = m_wood_sprites[number - 1];
            }
            else
            {
                tree.m_sprite_renderer.sprite = GetRandomTile();
                Destroy(tree_info);
            }
        }

        private Sprite GetRandomTile()
        {
            return m_tiles[Random.Range(0, m_tiles.Length - 1)];
        }
    }
}