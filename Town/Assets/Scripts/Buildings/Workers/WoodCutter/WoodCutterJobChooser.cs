﻿using System.Linq;
using Assets.Scripts.Commons;
using Assets.Scripts.GridFunctionProviders;
using Assets.Scripts.SpriteExtender;
using UnityEngine;
namespace Assets.Scripts.Buildings.Work
{
    using HexGrid.Hex;
    public sealed class WoodCutterJobChooser
    {
        private const float PREFERE_PLANTING = 0.25f;

        private readonly IBuildingInformation m_building;
        private readonly HexGrid m_building_hex_grid;
        private readonly int m_wood_sprites_count;
        public WoodCutterJobChooser(IBuildingInformation building, HexGrid building_hex_grid, int wood_sprites_count1)
        {
            m_building = building;
            m_building_hex_grid = building_hex_grid;
            m_wood_sprites_count = wood_sprites_count1;
        }

        public WoodCutterJob ChooseJob(out GridCellInfo target)
        {
            var tree_grids = m_building_hex_grid.GetGrinds(m_building.GetActionRange(), GetGridsWithTree);
            var empty_grids = m_building_hex_grid.GetGrinds(m_building.GetActionRange(), IsPlantable);

            var all_doable_grids = tree_grids.Count + empty_grids.Count;

            var tree_grids_percentage = (float)tree_grids.Count / all_doable_grids;

            if(tree_grids_percentage < PREFERE_PLANTING || tree_grids.Count == 0)
            {
                target = empty_grids.Random();
                return WoodCutterJob.plant_tree;
            }

            var random = Random.Range(0, 5);
            if(random == 1)
            {
                target = empty_grids.Random();
                return WoodCutterJob.plant_tree;
            }
            else if(random > 1 && random < 3)
            {
                target = tree_grids.Random();
                var sprite_number = SpriteNumber.GetNumber(target.GetSpriteName());
                if(IsNumberTheHighestPossible(sprite_number))
                {
                    return WoodCutterJob.cut_tree;
                }
                return WoodCutterJob.grow_tree;
            }
            else
            {
                target = tree_grids.Where(x => SpriteNumber.GetNumber(x.GetSpriteName()) > 0).Random();
                return WoodCutterJob.cut_tree;
            }
        }

        private bool IsNumberTheHighestPossible(int sprite_number)
        {
            return sprite_number >= m_wood_sprites_count - 1;
        }

        private bool IsPlantable(GridCellInfo grid)
        {
            return grid.tag == Tags.Tags.HEXGRID;
        }
        private bool GetGridsWithTree(GridCellInfo grid)
        {
            return grid.IsWood();
        }
        private static bool CheckIfTreeDoesntExist(GridCellInfo grid)
        {
            return grid == null;
        }
    }
}