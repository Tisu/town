﻿namespace Assets.Scripts.Buildings.Work
{
    public enum WoodCutterJob
    {
        plant_tree,
        grow_tree,
        cut_tree
    }
}