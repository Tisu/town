﻿using UnityEngine;

namespace Assets.Scripts.Buildings.Workers.House
{
    public sealed class HouseWorker : MonoBehaviour
    {
        private const uint POPULATION_PER_HOUSE = 5;
        void Awake()
        {
            ResourcesManager.m_instance.m_max_population += (int)POPULATION_PER_HOUSE;
        }

        void OnDestroy()
        {
            ResourcesManager.m_instance.m_max_population -= (int)POPULATION_PER_HOUSE;
        }
    }
}