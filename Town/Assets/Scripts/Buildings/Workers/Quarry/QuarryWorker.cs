﻿using System;
using Assets.Scripts.Commons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Buildings.Workers.Quarry
{
    public sealed class QuarryWorker : MonoBehaviour
    {
        private const float BRINDING_TIME = 10;
        private const float WORKER_RESTING_TIME = 35f;
        private const uint PRODUCTION = 3;

        public GameObject m_progress_bar { get; set; }
        public GameObject m_display { get; set; }
        public uint m_stone_production { get; set; }
        
        private IUpdatePeriodExecutor m_update_period_executor;
        private GridCellInfo m_building_grid_cell_info;
        private ProgressBar.ProgressBar m_progress_bar_script;

        private StoneState m_stone_state;
        public void StartWork()
        {
            m_stone_production = PRODUCTION;
            m_update_period_executor = new UpdatePeriodExecutor(WORKER_RESTING_TIME, DoWork, this);
            m_building_grid_cell_info = GetComponent<GridCellInfo>();
            m_stone_state = new StoneState(SpriteNumber.GetNumber(m_building_grid_cell_info));
        }
        void FixedUpdate()
        {
            m_update_period_executor.Update();
        }
        private void DoWork()
        {
            if (!m_stone_state.IsMineable())
            {
                DisplayOutOfResource();
                return;
            }
            Mine();
        }

        private void DisplayOutOfResource()
        {
            var display_message = Instantiate(m_display, transform.position,
                    Quaternion.identity) as GameObject;
            var text = display_message.GetComponentInChildren<Text>();
            text.text = "Out of " + Environment.NewLine + " resource";
        }

        private void Mine()
        {
            if (m_progress_bar_script == null)
            {
                InstantiateProgressBar();
            }
            SetMiningProgressBar();
            m_stone_state.MineResource();
        }

        private void OnMiningFinished()
        {
            m_progress_bar_script.m_progress_time = BRINDING_TIME;
            m_progress_bar_script.SetText("Carving");
            m_progress_bar_script.m_on_finished = OnGrindingFinished; 
        }
        private void OnGrindingFinished()
        {
            m_progress_bar_script.gameObject.SetActive(false);
            ResourcesManager.m_instance.m_stone += m_stone_production;
        }

        private void SetMiningProgressBar()
        {
            m_progress_bar_script.gameObject.SetActive(true);
            m_progress_bar_script.m_progress_time = 10f;
            m_progress_bar_script.SetText("Mining...");
        }
        private void InstantiateProgressBar()
        {
            var instantiated_progess_bar = Instantiate(m_progress_bar, transform.position, Quaternion.identity) as GameObject;
            instantiated_progess_bar.transform.SetParent(gameObject.transform);

            m_progress_bar_script = instantiated_progess_bar.GetComponent<ProgressBar.ProgressBar>();
            m_progress_bar_script.m_grid_cell_traget = m_building_grid_cell_info;
            m_progress_bar_script.SetProgressBarColor(Color.grey);

            m_progress_bar_script.DontDestroyOnFinish();
            m_progress_bar_script.m_on_finished = OnMiningFinished;
        }
    }
}