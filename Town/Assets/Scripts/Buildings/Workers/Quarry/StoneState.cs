﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Buildings.Workers.Quarry
{
    sealed class  StoneState
    {
        private readonly int[] QUARRY_DURATION = { 40, 100 };
        private int m_stone_state;
        private readonly int m_stone_sprite_index;
        public StoneState(int stone_sprite_index)
        {
            m_stone_sprite_index = stone_sprite_index;
        }
        public void MineResource()
        {
            m_stone_state++;
        }
        public bool IsMineable()
        {
            return m_stone_state <= QUARRY_DURATION[m_stone_sprite_index];
        }
    }
}
