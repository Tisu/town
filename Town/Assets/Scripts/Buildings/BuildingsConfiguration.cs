﻿namespace Assets.Scripts.Buildings
{
    public static class BuildingsConfiguration
    {
        public const int POPULATION_NEEDED_PER_BUILDING = 2;
        public static readonly string[] BUILDINGS_DOESNT_NEED_POPULATION =
        {
            BuildingsNames.HOUSE,
            BuildingsNames.FIELD
        };
    }
}