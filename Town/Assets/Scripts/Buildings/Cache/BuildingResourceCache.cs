﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Buildings
{
    public sealed class BuildingResourceCache : MonoBehaviour, IBuildingResourceCache
    {
        [SerializeField]
        private Sprite[] m_wood_sprites;
        [SerializeField]
        private Sprite[] m_tiles;
        [SerializeField]
        private GameObject m_progress_bar;
        [SerializeField]
        private GameObject m_diplay_object_message;
        public Sprite[] GetWoodSprites()
        {
            return m_wood_sprites;
        }
        public Sprite[] GetTilesSprites()
        {
            return m_tiles;
        }
        public GameObject GetProgressBar()
        {
            return m_progress_bar;
        }
        public GameObject GetDisplayMessageObect()
        {
            return m_diplay_object_message;
        }
    }
}