﻿namespace Assets.Scripts.Buildings
{
    public sealed class BuildingsNames
    {
        public const string CHURCH = "Church";
        public const string MARKET = "Market";
        public const string WOODS_CUTTER = "Woodcutter's";
        public const string QUARRY = "Quarry";
        public const string CITY_HALL = "City Hall";
        public const string TOWER = "Tower";
        public const string RUINS = "Ruins";
        public const string ARCHWAY = "Archway";
        public const string HOUSE = "House";
        public const string FARM = "Farm";
        public const string FIELD = "Field";
    }
}