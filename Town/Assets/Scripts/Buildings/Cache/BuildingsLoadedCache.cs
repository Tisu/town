﻿
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.AssetsManagement;
using Assets.Scripts.AssetsManagement.Caches.Building;

static class BuildingsLoadedCache
{
    public static List<IBuildingInformation> m_cache { get; private set; }

    static BuildingsLoadedCache()
    {
        m_cache = new List<IBuildingInformation>();
        var asset_buildings_cache = LoadCache.LoadBuildings();
        foreach (var building in asset_buildings_cache.m_cache)
        {
            BuildingInformation copied_building = building.Clone();
            m_cache.Add(copied_building);
        }
    }

    public static IBuildingInformation GetBuildingBySpriteName(string name)
    {
        return m_cache.FirstOrDefault(x => x.GetSpriteName() == name);
    }
    public static IBuildingInformation GetBuildingByName(string name)
    {
        return m_cache.FirstOrDefault(x => x.GetName().Trim() == name);
    }
}

