﻿using UnityEngine;

namespace Assets.Scripts.Buildings
{
    public interface IBuildingResourceCache
    {
        GameObject GetProgressBar();
        Sprite[] GetTilesSprites();
        Sprite[] GetWoodSprites();
        GameObject GetDisplayMessageObect();
    }
}