﻿using UnityEngine;

namespace Assets.Scripts.Buildings
{
    public interface IBuildingWorkerBuilder
    {
        void AddBuildingWorker(GameObject grid, IBuildingInformation building);
    }
}