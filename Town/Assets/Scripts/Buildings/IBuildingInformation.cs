﻿using Assets.Scripts.AssetsManagement.Caches.Building;
using UnityEngine;

public interface IBuildingInformation
{
    string GetDescription();
    string GetName();
    Sprite GetSprite();
    string GetSpriteName();
    uint GetStoneCost();
    uint GetWoodCost();
    int GetActionRange();
    string GetResourceDependency();
    float GetBuildingTime();
}