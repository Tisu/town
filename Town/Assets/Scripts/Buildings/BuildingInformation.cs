﻿using System;
using UnityEngine;

namespace Assets.Scripts.AssetsManagement.Caches.Building
{
    public sealed class BuildingInformation : IBuildingInformation
    {
        public string m_name;
        public int m_wood_cost;
        public int m_stone_cost;
        public Sprite m_sprite;
        public string m_description;
        public Technology m_technology_to_unlock;
        public int m_action_range;
        public string m_resource_dependency;
        public float m_building_time;
       
        public string GetSpriteName()
        {
            if(m_sprite == null)
            {
                throw new Exception("Sprite name not set");
            }
            return m_sprite.name;
        }
        public uint GetStoneCost()
        {
            return (uint)m_stone_cost;
        }
        public uint GetWoodCost()
        {
            return (uint)m_wood_cost;
        }
        public int GetActionRange()
        {
            return m_action_range;
        }
        public string GetResourceDependency()
        {
            return m_resource_dependency;
        }
        public float GetBuildingTime()
        {
            return m_building_time;
        }
        public string GetName()
        {
            return m_name;
        }
        public Sprite GetSprite()
        {
            return m_sprite;
        }
        public string GetDescription()
        {
            return m_description;
        }
    }
}