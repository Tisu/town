﻿using System;
using Assets.Scripts.Commons;
using Assets.Scripts.GridFunctionProviders;
using Assets.Scripts.MainMenu;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Food
{
    public sealed class FoodManager : MonoBehaviour
    {
        [SerializeField]
        private Sprite m_dirt;
        [SerializeField]
        private GameObject m_progress_bar;
        [SerializeField]
        private GameObject m_game_over;
        [SerializeField]
        private MenuInGame m_menu_in_game;

        public const int FOOD_CONSUMPTION_PER_PERSON = 1;
        private const int CONSUMPTION_PERIOD = 70;
        private IUpdatePeriodExecutor m_update_period_executor;
        
        void Awake()
        {
            m_update_period_executor = new UpdatePeriodExecutor(CONSUMPTION_PERIOD, ConsumeFood, this, 120);
        }
        void FixedUpdate()
        {
            m_update_period_executor.Update();
        }
        private void ConsumeFood()
        {
            var consumption = FOOD_CONSUMPTION_PER_PERSON * (int)ResourcesManager.m_instance.m_population;
            ResourcesManager.m_instance.m_food -= consumption;
            if(ResourcesManager.m_instance.m_food <= 0)
            {
                if (ResourcesManager.m_instance.m_food <= -30)
                {
                    m_game_over.SetActive(true);
                    m_menu_in_game.GameOver();
                    return;
                }
                DestroyBuilding();
            }
        }

        private void DestroyBuilding()
        {
            foreach(var grid in GridGameController.m_instance)
            {
                if(grid.tag != Tags.Tags.BUILDING)
                {
                    Debug.Log("test");
                    continue;
                }
                var components =  grid.GetComponents<MonoBehaviour>();
                foreach (var component in components)
                {
                    if (component is GridCellInfo)
                    {
                        continue;
                    }
                    Destroy(component);
                }
                grid.m_sprite_renderer.sprite = m_dirt;
                grid.tag = Tags.Tags.HEXGRID;
                var progress_bar = Instantiate(m_progress_bar, grid.transform.position, Quaternion.identity) as GameObject;
                var script = progress_bar.GetComponent<ProgressBar.ProgressBar>();
                script.m_progress_time = 5;
                script.m_grid_cell_traget = grid;
                script.SetProgressBarColor(Color.black);
                script.SetText("Abandoned");
            }
        }
    }
}