﻿using System;
using System.Collections.Generic;
class InventoryIndexComparer : IEqualityComparer<InventoryBackgroundIndex>
{
    public bool Equals(InventoryBackgroundIndex x, InventoryBackgroundIndex y)
    {
        return x.m_x == y.m_x && x.m_y == y.m_y;
    }

    public int GetHashCode(InventoryBackgroundIndex obj)
    {
        return base.GetHashCode();
    }
}

