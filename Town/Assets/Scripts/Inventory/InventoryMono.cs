﻿using Assets.Scripts.Inventory.FirstBuildingMounter;
using UnityEngine;

public class InventoryMono : MonoBehaviour, ICustomEditor
{
    [SerializeField]
    Sprite m_background_block;
    [SerializeField]
    private Sprite m_changing_inventory_sprite;

    [SerializeField]
    private Canvas m_canvas;

    private readonly InventoryManager m_inventory_manager = new InventoryManager();

    void Awake()
    {
        m_inventory_manager.GenerateBackground(m_background_block, m_changing_inventory_sprite, m_canvas);
        new FirstBuildingMounter().MountFirstBuilding(m_inventory_manager);
    }

    public void Generate()
    {
        m_inventory_manager.GenerateBackground(m_background_block, m_changing_inventory_sprite, m_canvas);
    }
    public void MountItem(Sprite a_sprite)
    {
        m_inventory_manager.MountItem(a_sprite);
    }
    public void MountItem(string sprite_name)
    {
        var building = BuildingsLoadedCache.GetBuildingBySpriteName(sprite_name);
        if(building == null)
        {
            Debug.LogError("Not added to cache!");
            return;
        }
        m_inventory_manager.MountItem(building.GetSprite());
    }
}
