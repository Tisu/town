﻿using UnityEngine;
public interface IInventoryBackground
{
    GameObject m_background { get; }
    void MountItem(GameObject a_gameobject,int a_w);
    void MountItem(Sprite a_sprite, int a_w);
    void DeleteItem();
    void SetNotActive(int a_w);
    void SetActive(int a_w);
    void SetVisible(int a_w);
    /// <summary>
    /// When resources are ok set to green
    /// </summary>
    void SetUseable();
    /// <summary>
    /// when not enough resources cannot be build/ set to red
    /// </summary>
    void SetUnUsable();
   
}

