﻿
namespace Assets.Scripts.Inventory
{
    public static class InventoryConfig
    {
        public const float SQUARE_SIDE = 60f;
        public const float SPRITE_SIZE = 55f;
        public const float THRESHOLD = SQUARE_SIDE / 2;
        public const float BACKGROUND_Z = 2;
        public const float THRESHOLD_Y = 10f;

        public const float ARROW_SPRITE_SIZE = SQUARE_SIDE;
        public const float ARROW_SPRITE_SIZE_X = 40;
        public const float ARROW_THRESHOLD = SQUARE_SIDE / 2 + ARROW_SPRITE_SIZE_X / 2 + 5f;
        public const uint MAX_INVENTORY_BACKGROUND = 10;
    }
}