﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Inventory;
using Assets.Scripts.Tags;
using UnityEngine;
using UnityEngine.UI;

class InventoryBackground : IInventoryBackground, IInventoryTap
{
    public InventoryBackgroundIndex m_inventory_index { get; private set; }
    public GameObject m_background { get; private set; }

    static readonly Color32 m_green_background_ok = new Color32(45, 255, 73, 255);
    static readonly Color32 m_red_background_warning = new Color32(233, 45, 0, 255);

    readonly Color32 m_normal_background;
    readonly Image m_background_spriterenderer;
    GameObject m_current_mounted_object;

    readonly Dictionary<int, GameObject> m_inventory_cache = new Dictionary<int, GameObject>();

    public InventoryBackground()
    {

    }
    public InventoryBackground(InventoryBackgroundIndex a_index, GameObject a_background)
    {
        m_inventory_index = a_index;
        m_background = a_background;
        m_background_spriterenderer = a_background.GetComponent<Image>();
        m_normal_background = m_background_spriterenderer.color;
    }

    public void DeleteItem()
    {
        if(m_inventory_cache.Count > 0)
        {
            foreach(var item in m_inventory_cache.Values)
            {
                Object.Destroy(item);
            }
        }
    }
    public void MountItem(GameObject a_gameobject, int a_w)
    {
        a_gameobject.transform.position = m_background.transform.position;
        a_gameobject.transform.SetParent(m_background.transform);
        m_inventory_cache[a_w] = a_gameobject;
        m_current_mounted_object = a_gameobject;
    }

    public void MountItem(Sprite a_sprite, int a_w)
    {
        if(a_sprite == null)
        {
            Tisu.Logger.LogError("Sprite is null ! ");
            return;
        }
        var item = new GameObject();
        var item_renderer = item.AddComponent<Image>();
        item_renderer.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, InventoryConfig.SPRITE_SIZE);
        item_renderer.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, InventoryConfig.SPRITE_SIZE);
        item_renderer.sprite = a_sprite;
        item.name = a_sprite.name;
        MountItem(item, a_w);
    }

    public void SetVisible(int a_w)
    {
        m_current_mounted_object = null;
        for(int i = 0; i < m_inventory_cache.Count; i++)
        {
            if(i == a_w)
            {
                SetActive(i);
            }
            else
            {
                SetNotActive(i);
            }
        }
    }
    public void SetNotActive(int a_w)
    {
        m_inventory_cache[a_w].SetActive(false);
    }

    public void SetActive(int a_w)
    {
        m_current_mounted_object = m_inventory_cache[a_w];
        m_current_mounted_object.SetActive(true);
    }

    public void SetUseable()
    {
        if(m_current_mounted_object != null)
        {
            m_background_spriterenderer.color = m_green_background_ok;
        }
    }

    public void SetUnUsable()
    {
        if(m_current_mounted_object != null)
        {
            m_background_spriterenderer.color = m_red_background_warning;
        }
    }

    public string GetMountedSpriteName()
    {
        return m_current_mounted_object == null ? "" : m_current_mounted_object.name;
    }

    public void OnTap()
    {
        if (m_current_mounted_object == null)
        {
            return;
        }
        ResourcesManager.m_instance.m_change_resources_callback += ChangeInventoryDisplay;
        ChangeInventoryDisplay();
    }

    private void ChangeInventoryDisplay()
    {
        var building_cost = BuildingsLoadedCache.GetBuildingBySpriteName(m_current_mounted_object.name);
        if (ResourceChecker.CheckResources(building_cost))
        {
            SetUseable();
        }
        else
        {
            SetUnUsable();
        }
    }

    public void DisableLastTap()
    {
        ResourcesManager.m_instance.m_change_resources_callback -= ChangeInventoryDisplay;
        SetNormal();
    }
    private void SetNormal()
    {
        m_background_spriterenderer.color = m_normal_background;
    }

    public void CallInputProvider()
    {
    }
}

