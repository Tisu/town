﻿using UnityEngine;

interface IInventoryManager
{
    int m_current_inventory_w { get; set; }
    void GenerateBackground(Sprite a_sprite, Sprite change_inventory_icon, Canvas canvas);
    void MountItem(Sprite a_sprite);
}