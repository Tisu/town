﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Inventory
{
    public sealed class InventoryGenerator : IGenerateInventory
    {
        private readonly InventoryItemManager m_inventory_item_manager;
        private InputProvider m_input_provider;
        public Button m_left_button { get; private set; }
        public Button m_right_button { get; private set; }
        public InventoryGenerator(InventoryItemManager inventory_item_manager)
        {
            m_inventory_item_manager = inventory_item_manager;
            m_input_provider = GameObject.Find("InputProvider").GetComponent<InputProvider>();
        }

        public void GenerateBackground(Sprite a_sprite, Sprite change_inventory_icon, Canvas canvas, InventoryManager inventory_manager)
        {
            var inventory_parent = new GameObject { name = "Inventory_background" };
            var parent = inventory_parent.AddComponent<RectTransform>();
            parent.anchorMax = new Vector2(0.5f, 0);
            parent.anchorMin = new Vector2(0.5f, 0);

            var rect = canvas.GetComponent<RectTransform>();
            var screen_size = new Vector2(rect.rect.width,
                rect.rect.height);

            var predicted_colums = (uint)((screen_size.x - InventoryConfig.THRESHOLD - InventoryConfig.ARROW_SPRITE_SIZE) / InventoryConfig.SQUARE_SIDE);
            m_inventory_item_manager.m_colums = predicted_colums > InventoryConfig.MAX_INVENTORY_BACKGROUND ? InventoryConfig.MAX_INVENTORY_BACKGROUND : predicted_colums;
            m_inventory_item_manager.m_rows = 1;

            float offset_x = (screen_size.x - InventoryConfig.SQUARE_SIDE * m_inventory_item_manager.m_colums) / 2;
            var offset = new Vector2(InventoryConfig.THRESHOLD - screen_size.x + rect.transform.position.x + offset_x,
                InventoryConfig.THRESHOLD - screen_size.y + rect.transform.position.y + InventoryConfig.THRESHOLD_Y);

            for(int y = 0; y < m_inventory_item_manager.m_rows; y++)
            {
                for(int x = 0; x < m_inventory_item_manager.m_colums; x++)
                {
                    SpawnBackground(a_sprite, inventory_parent, offset, y, x);
                }
            }
            SpawnArrors(offset, change_inventory_icon, inventory_parent.transform, inventory_manager);

            inventory_parent.transform.SetParent(canvas.transform);
            inventory_parent.transform.localPosition = new Vector3(inventory_parent.transform.position.x, inventory_parent.transform.position.y, InventoryConfig.BACKGROUND_Z);
        }

        private void SpawnArrors(Vector2 offset, Sprite image, Transform parent, InventoryManager inventory_manager)
        {
            SpawnLeftArrow(offset, image, parent, inventory_manager);

            SpawnRightArrow(offset, image, parent, inventory_manager);
        }

        private void SpawnRightArrow(Vector2 offset, Sprite image, Transform parent, InventoryManager inventory_manager)
        {
            var columns = m_inventory_item_manager.m_colums - 1;
            var arrow = new GameObject();
            arrow.transform.SetParent(parent);
            var sprite_image = arrow.AddComponent<Image>();

            sprite_image.rectTransform.anchorMax = new Vector2(0.5f, 0);
            sprite_image.rectTransform.anchorMin = new Vector2(0.5f, 0);

            arrow.transform.position = new Vector3(offset.x + columns * InventoryConfig.SQUARE_SIDE + InventoryConfig.ARROW_THRESHOLD, offset.y, 0);
            var collider = arrow.AddComponent<BoxCollider2D>();
            collider.size = new Vector2(InventoryConfig.ARROW_SPRITE_SIZE_X, InventoryConfig.ARROW_SPRITE_SIZE / 2 + 20);

            m_right_button = arrow.AddComponent<Button>();
            m_right_button.onClick.AddListener(inventory_manager.OnRightButtonClick);

            sprite_image.sprite = image;
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                InventoryConfig.ARROW_SPRITE_SIZE_X);
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, InventoryConfig.ARROW_SPRITE_SIZE);
            arrow.tag = Tags.Tags.INVENTORY;
        }

        private void SpawnLeftArrow(Vector2 offset, Sprite image, Transform parent, InventoryManager inventory_manager)
        {
            var arrow = new GameObject();
            arrow.transform.SetParent(parent);
            var sprite_image = arrow.AddComponent<Image>();
            arrow.transform.localScale = new Vector3(-1, 1, 1);

            sprite_image.rectTransform.anchorMax = new Vector2(0.5f, 0);
            sprite_image.rectTransform.anchorMin = new Vector2(0.5f, 0);

            arrow.transform.position = new Vector3(offset.x - InventoryConfig.ARROW_THRESHOLD, offset.y, 0);

            var collider = arrow.AddComponent<BoxCollider2D>();
            collider.size = new Vector2(InventoryConfig.ARROW_SPRITE_SIZE_X, InventoryConfig.ARROW_SPRITE_SIZE / 2 + 20);

            m_left_button = arrow.AddComponent<Button>();
            m_left_button.onClick.AddListener(inventory_manager.OnLeftButtonClick);

            sprite_image.sprite = image;
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                InventoryConfig.ARROW_SPRITE_SIZE_X);
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, InventoryConfig.ARROW_SPRITE_SIZE);
            arrow.tag = Tags.Tags.INVENTORY;
        }

        private void SpawnBackground(Sprite a_sprite, GameObject a_inventory_parent, Vector2 a_offset, int a_y, int a_x)
        {
            var inventory_background_gameobject = new GameObject();
            var sprite_image = inventory_background_gameobject.AddComponent<Image>();
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, InventoryConfig.SQUARE_SIDE);
            sprite_image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, InventoryConfig.SQUARE_SIDE);
            sprite_image.rectTransform.anchorMax = new Vector2(0.5f, 0);
            sprite_image.rectTransform.anchorMin = new Vector2(0.5f, 0);

            inventory_background_gameobject.tag = Tags.Tags.INVENTORY;

            sprite_image.sprite = a_sprite;
            inventory_background_gameobject.transform.SetParent(a_inventory_parent.transform);
            inventory_background_gameobject.transform.position = new Vector3(a_x * InventoryConfig.SQUARE_SIDE + a_offset.x, a_y * InventoryConfig.SQUARE_SIDE + a_offset.y, 0);
            var collider = inventory_background_gameobject.AddComponent<BoxCollider2D>();
            collider.size = new Vector2(InventoryConfig.SQUARE_SIDE, InventoryConfig.SQUARE_SIDE);
            var index = new InventoryBackgroundIndex(a_x, a_y);
            var inventory_background = new InventoryBackground(index, inventory_background_gameobject);
            m_inventory_item_manager.m_inventory_background_cache.Add(index, inventory_background);

            var background_tap = inventory_background_gameobject.AddComponent<InventoryBackgroundTap>();
            background_tap.m_input_provider = m_input_provider;
            inventory_background_gameobject.AddComponent<Button>().onClick.AddListener(background_tap.CallInputProvider);
            background_tap.m_inventory_backtround = inventory_background;
        }
    }
}