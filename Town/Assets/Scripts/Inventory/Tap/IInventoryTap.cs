﻿
interface IInventoryTap
{
    void OnTap();
    void DisableLastTap();
    void CallInputProvider();
    string GetMountedSpriteName();
}

