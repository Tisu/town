﻿using UnityEngine;

class InventoryBackgroundTap : MonoBehaviour, IInventoryTap
{
    public InventoryBackground m_inventory_backtround;
    public InputProvider m_input_provider;
    
    public void CallInputProvider()
    {
        m_input_provider.RunActionClick(GetComponent<Collider2D>());
    }

    public string GetMountedSpriteName()
    {
        return m_inventory_backtround.GetMountedSpriteName();
    }

    public void DisableLastTap()
    {
        m_inventory_backtround.DisableLastTap();
    }

    public void OnTap()
    {
        m_inventory_backtround.OnTap();
    }
}

