﻿using System.Collections.Generic;
using UnityEngine;

public interface IInventoryItemManager
{
    Dictionary<InventoryBackgroundIndex, IInventoryBackground> m_inventory_background_cache { get; }
    bool GetInventoryItem(InventoryBackgroundIndex a_index, out IInventoryBackground a_item);
    void MountItem(Sprite a_sprite, int a_current_w);
    bool IsEverySlotOccupied(int a_current_w);
}