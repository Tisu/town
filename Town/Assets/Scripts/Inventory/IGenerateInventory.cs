﻿using UnityEngine;

namespace Assets.Scripts.Inventory
{
    public interface IGenerateInventory
    {
        void GenerateBackground(Sprite a_sprite, Sprite change_inventory_icon, Canvas canvas, InventoryManager inventory_manager);
    }
}