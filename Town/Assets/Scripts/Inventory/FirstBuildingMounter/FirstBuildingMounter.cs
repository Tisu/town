﻿using System;
using Assets.Scripts.Buildings;

namespace Assets.Scripts.Inventory.FirstBuildingMounter
{
    public sealed class FirstBuildingMounter
    {
        public void MountFirstBuilding(InventoryManager manager)
        {
            MountSpriteWithName(BuildingsNames.WOODS_CUTTER, manager);
            MountSpriteWithName(BuildingsNames.QUARRY, manager);
            MountSpriteWithName(BuildingsNames.CITY_HALL, manager);
            MountSpriteWithName(BuildingsNames.HOUSE, manager);
        }

        private void MountSpriteWithName(string name, InventoryManager manager)
        {
            var building = BuildingsLoadedCache.GetBuildingByName(name);
            if(building == null)
            {
                throw new Exception("Add building to cache!" + name);
            }
            manager.MountItem(building.GetSprite());
        }
    }
}