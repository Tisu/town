﻿using Assets.Scripts.Inventory;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : IInventoryManager
{
    private readonly IInventoryItemManager m_inventory = new InventoryItemManager();
    private int m_current_inventory_w_p;

    private Button m_left_button;
    private Button m_right_button;
    public int m_current_inventory_w
    {
        get
        {
            return m_current_inventory_w_p;
        }
        set
        {
            ChangeVisibleInventory(value);
            m_current_inventory_w_p = value;
            EnableButtons();
        }
    }

    public void OnLeftButtonClick()
    {
        m_current_inventory_w--;
    }

    public void OnRightButtonClick()
    {
        m_current_inventory_w++;
    }
    public void GenerateBackground(Sprite a_sprite, Sprite change_inventory_icon, Canvas canvas)
    {
        var inventory_generator = new InventoryGenerator(m_inventory as InventoryItemManager);
        inventory_generator.GenerateBackground(a_sprite, change_inventory_icon, canvas, this);
        m_left_button = inventory_generator.m_left_button;
        m_right_button = inventory_generator.m_right_button;

        m_left_button.interactable = false;
        m_right_button.interactable = false;
    }
    public void MountItem(Sprite a_sprite)
    {
        m_inventory.MountItem(a_sprite, m_current_inventory_w_p);
        EnableButtons();
    }
    private void EnableButtons()
    {
        m_right_button.interactable = CheckIfRightButtonShouldBeEnabled();
        m_left_button.interactable = CheckIfLeftButtonShouldBeEnabled();
    }
    private bool CheckIfRightButtonShouldBeEnabled()
    {
        return m_inventory.IsEverySlotOccupied(m_current_inventory_w_p);
    }
    private bool CheckIfLeftButtonShouldBeEnabled()
    {
        return m_current_inventory_w_p > 0;
    }
    private void ChangeVisibleInventory(int a_new_w)
    {
        if(m_current_inventory_w_p != a_new_w)
        {
            foreach(var background in m_inventory.m_inventory_background_cache.Values)
            {
                background.SetVisible(a_new_w);
            }
        }
    }
}

