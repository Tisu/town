﻿
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemManager : IInventoryItemManager
{
    public Dictionary<InventoryBackgroundIndex, IInventoryBackground> m_inventory_background_cache { get; private set; }

    public uint m_colums;
    public uint m_rows;

    private int m_next_spawn_w;
    private InventoryBackgroundIndex m_next_mounted_index = new InventoryBackgroundIndex(0, 0);
    private List<string> m_mounted_sprites = new List<string>();

    public InventoryItemManager()
    {
        m_inventory_background_cache = new Dictionary<InventoryBackgroundIndex, IInventoryBackground>(new InventoryIndexComparer());
    }
    public bool GetInventoryItem(InventoryBackgroundIndex a_index, out IInventoryBackground a_item)
    {
        return m_inventory_background_cache.TryGetValue(a_index, out a_item);
    }
    public void MountItem(Sprite a_sprite, int a_current_w)
    {
        if(m_mounted_sprites.Contains(a_sprite.name))
        {
            Debug.Log("Should not happened! sprite with " + a_sprite.name + " is already mounted! ");
            return;
        }
        IInventoryBackground item;
        if(m_inventory_background_cache.TryGetValue(m_next_mounted_index, out item))
        {
            item.MountItem(a_sprite, m_next_spawn_w);
            m_mounted_sprites.Add(a_sprite.name);
            if(m_next_spawn_w != a_current_w)
            {
                item.SetNotActive(m_next_spawn_w);
            }
            NextIndex();
        }
        else
        {
            Tisu.Logger.LogError("Wrong Mount!");
        }
    }

    public bool IsEverySlotOccupied(int current_w)
    {
        return m_next_spawn_w != current_w;
    }

    private void NextIndex()
    {
        int x = m_next_mounted_index.m_x;
        int y = m_next_mounted_index.m_y;

        x = x + 1 > m_colums - 1 ? 0 : x += 1;
        if(x == 0)
        {
            if(y == m_rows - 1)
            {
                m_next_spawn_w++;
            }
            else
            {
                y = y + 1 > m_rows ? 0 : y + 1;
            }
        }
        if(y > m_rows)
        {
            Tisu.Logger.LogError("Wrong calculation!");
        }
        m_next_mounted_index = new InventoryBackgroundIndex(x, y);
    }
}


