﻿using UnityEditor;
using UnityEngine;

public static class GenericAssetManager<T> where T : ScriptableObject
{
    public static T LoadWithCreate(string a_file_name)
    {
        var sprites = ScriptableObject.CreateInstance<T>();
        Load(ref sprites, a_file_name);
        return sprites;
    }
    public static void Load(ref T a_cache, string a_file_name)
    {
        a_cache = AssetDatabase.LoadAssetAtPath<T>(DatabaseInformation.DATABASE_PATH + a_file_name);
        if(a_cache == null)
        {
            if(!AssetDatabase.IsValidFolder(DatabaseInformation.DATABASE_PATH_WITHOUT_SEPARATOR))
            {
                AssetDatabase.CreateFolder(DatabaseInformation.RESOURCES_PATH_WITHOUT_SEPARATOR, DatabaseInformation.CUSTOM_DATABASE_FOLDER);
            }
            a_cache = ScriptableObject.CreateInstance<T>();
            CreateAsset(a_cache, ref a_file_name);
        }
    }
    private static void CreateAsset(T a_cache, ref string a_file_name)
    {
        AssetDatabase.CreateAsset(a_cache, DatabaseInformation.DATABASE_PATH + a_file_name);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}

