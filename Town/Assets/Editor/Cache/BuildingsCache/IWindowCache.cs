﻿interface IWindowCache
{
    void AddNewToCache();
    void OnDestroy();
    void OnEnable();
}