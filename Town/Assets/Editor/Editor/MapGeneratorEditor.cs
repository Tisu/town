﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpawnGridMono))]
public class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        SpawnGridMono grid_spawner = (SpawnGridMono)target;
        if (DrawDefaultInspector() && grid_spawner.m_auto_update)
        {
            grid_spawner.Generate();
        }
        if (GUILayout.Button("Generate"))
        {
            grid_spawner.Generate();
        }
        if (GUILayout.Button("Map generation window"))
        {
           EditorWindow.GetWindow<MapGenerateWindow>();
        }
    }
}
