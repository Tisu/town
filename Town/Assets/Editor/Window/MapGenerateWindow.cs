﻿using Assets.Scripts.AssetsManagement;
using UnityEditor;
using UnityEngine;

public class MapGenerateWindow : EditorWindow
{
    SpriteCache m_sprite_cache;

    [MenuItem("Tisu/MapGeneration")]
    static void Init()
    {
        var window = GetWindow<MapGenerateWindow>();
        window.minSize = new Vector2(800, 500);
        window.Show();
        window.wantsMouseMove = true;
    }
    public void OnEnable()
    {
        GenericAssetManager<SpriteCache>.Load(ref m_sprite_cache, CacheNames.SPRITES + "Cache.asset");
    }

    public void OnGUI()
    {
        if (Event.current.type == EventType.MouseUp)
        {
            m_sprite_cache.Sort();
            Repaint();
        }
        GUILayout.Label("Terrain sprite and its value 0-1", EditorStyles.boldLabel);
        if (GUILayout.Button("Add new sprite"))
        {
            m_sprite_cache.CreateNewSprite();
        }
        if(GUILayout.Button("Delete last one"))
        {
            m_sprite_cache.DeleteLastRow();
        }
        m_sprite_cache.OnGUI();
        EditorUtility.SetDirty(m_sprite_cache);
    }
}
