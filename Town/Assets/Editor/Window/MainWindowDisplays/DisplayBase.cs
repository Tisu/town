﻿
using UnityEditor;
using UnityEngine;

abstract class DisplayBase<T, U> where T : CacheBase<U> where U : ScriptableObject, ICacheName
{
    [SerializeField]
    readonly string ASSET_FILE_NAME;
    protected T m_cache_container;
    protected U m_current_selected;
    public abstract string ButtonEndName();
    public abstract void DisplayProperties();
    public DisplayBase(string a_display_name)
    {
        ASSET_FILE_NAME = a_display_name + @"Cache.asset";
    }
    public void DeleteCurrent()
    {
        m_cache_container.DeleteCurrent(ref m_current_selected);
    }
    public void DisplayListView()
    {
        if (m_cache_container.OnGui(ref m_current_selected))
        {
            GUI.SetNextControlName("");
            GUI.FocusControl("");
        }
    }
    public void OnDestroy()
    {
        m_cache_container.OnDestroy();
    }
    public void OnEnable()
    {
        if (m_cache_container == null)
        {
            GenericAssetManager<T>.Load(ref m_cache_container, ASSET_FILE_NAME);
        }
        EditorUtility.SetDirty(m_cache_container);
    }
    public void AddNewToCache()
    {
        m_cache_container.AddNewToCache();
    }
}

