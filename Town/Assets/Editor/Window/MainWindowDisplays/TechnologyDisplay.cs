﻿using System;
using UnityEditor;
using UnityEngine;
using System.Linq;
using Assets.Scripts.AssetsManagement;

class TechnologyDisplay : DisplayBase<TechnologyCache, Technology>, IWindowDisplay, IOtherWidnowCache<TechnologyCache>
{
    public TechnologyDisplay() : base(CacheNames.TECHNOLOGY)
    {

    }
    public override string ButtonEndName()
    {
        return CacheNames.TECHNOLOGY;
    }

    public override void DisplayProperties()
    {
        if (m_current_selected == null)
        {
            return;
        }
        GUILayout.Label("Name");
        m_current_selected.m_name = EditorGUILayout.TextField(m_current_selected.m_name);

        GUILayout.Label("Wood cost");
        m_current_selected.m_resource.m_wood_cost = (uint)EditorGUILayout.IntField(Convert.ToInt32(m_current_selected.m_resource.m_wood_cost));

        GUILayout.Label("Stone cost");
        m_current_selected.m_resource.m_stone_cost = (uint)EditorGUILayout.IntField((int)m_current_selected.m_resource.m_stone_cost);

        string[] technology_options;
        if (m_cache_container.m_cache.Count > 0)
        {
            technology_options = new string[m_cache_container.m_cache.Count];
            for (int i = 0; i < technology_options.Length; i++)
            {
                technology_options[i] = m_cache_container.m_cache.ElementAt(i).m_name;
            }
            m_current_selected.m_next_technology_index = EditorGUILayout.Popup("Prevous Technology ", m_current_selected.m_next_technology_index, technology_options);
            m_current_selected.m_previous_technology_index = EditorGUILayout.Popup("Next Technology", m_current_selected.m_previous_technology_index, technology_options);

            m_current_selected.m_next_technology = m_cache_container.m_cache.ElementAt(m_current_selected.m_next_technology_index);
            m_current_selected.m_previous_technology = m_cache_container.m_cache.ElementAt(m_current_selected.m_previous_technology_index);
        }
    }

    public TechnologyCache GetCache()
    {
        return m_cache_container;
    }

    public void SetOtherWindowCache(TechnologyCache a_cache)
    {
        throw new NotImplementedException();
    }
}

