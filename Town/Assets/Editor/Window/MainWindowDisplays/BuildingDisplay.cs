﻿
using System;
using System.Linq;
using Assets.Scripts.AssetsManagement;
using Assets.Scripts.AssetsManagement.Caches.Building;
using UnityEditor;
using UnityEngine;

class BuildingDisplay : DisplayBase<BuildingsCache, BuildingInformationAsset>, IWindowDisplay, IOtherWidnowCache<TechnologyCache>
{
    private const float BOARD_PADDING = 10f;
    private const float TEXT_FIELD_HEIGTH = 50f;
    TechnologyCache m_other_window_cache;

    public BuildingDisplay() : base(CacheNames.BUILDINGS)
    {

    }
    public override string ButtonEndName()
    {
        return CacheNames.BUILDINGS;
    }
    public override void DisplayProperties()
    {
        if(m_current_selected == null)
        {
            return;
        }
        GUILayout.Label("Name");
        m_current_selected.m_name = EditorGUILayout.TextField(m_current_selected.m_name);

        GUILayout.Label("Wood cost");
        m_current_selected.m_wood_cost = EditorGUILayout.IntField(m_current_selected.m_wood_cost);

        GUILayout.Label("Stone cost");
        m_current_selected.m_stone_cost = EditorGUILayout.IntField(m_current_selected.m_stone_cost);

        GUILayout.Label("Action Range");
        m_current_selected.m_action_range = EditorGUILayout.IntField(m_current_selected.m_action_range);

        var chosen_index = Array.FindIndex(ObjectType.ARRAY, x => x == m_current_selected.m_resource_dependency);
        chosen_index = chosen_index == -1 ? 0 : chosen_index;
        var index = EditorGUILayout.Popup("Resource dependency", chosen_index, ObjectType.ARRAY);
        if(index < ObjectType.ARRAY.Length && index >= 0)
        {
            m_current_selected.m_resource_dependency = ObjectType.ARRAY[index];
        }
        GUILayout.Label("Building Time [s]");
        m_current_selected.m_building_time = EditorGUILayout.FloatField(m_current_selected.m_building_time);

        GUILayout.Label("Description");

        m_current_selected.m_description = EditorGUILayout.TextArea
            (m_current_selected.m_description,
            GUILayout.MaxHeight(TEXT_FIELD_HEIGTH),
            GUILayout.ExpandHeight(false),
            GUILayout.MaxWidth(MainWindow.MAX_WINDOW_WIDTH - MainWindow.MAX_SCROLLBAR_WIDTH - BOARD_PADDING));

        if(CheckOtherWindowContainer())
        {
            var technology_options = new string[m_other_window_cache.m_cache.Count];
            for(int i = 0; i < technology_options.Length; i++)
            {
                technology_options[i] = m_other_window_cache.m_cache.ElementAt(i).m_name;
            }
            m_current_selected.m_selected_technology_index = EditorGUILayout.Popup("Technology to unlock building_information ", m_current_selected.m_selected_technology_index, technology_options);
            m_current_selected.m_technology_to_unlock = m_other_window_cache.m_cache.ElementAt(m_current_selected.m_selected_technology_index);
        }

        GUILayout.Label("Sprite");
        m_current_selected.m_sprite = EditorGUILayout.ObjectField(m_current_selected.m_sprite, typeof(Sprite), true) as Sprite;
        if(m_current_selected.m_sprite != null)
        {
            Rect sprite_rectangle = m_current_selected.m_sprite.rect;
            Rect rect = GUILayoutUtility.GetRect(sprite_rectangle.width, sprite_rectangle.height * 2);
            if(Event.current.type == EventType.Repaint)
            {
                var sprite_texture = m_current_selected.m_sprite.texture;
                sprite_rectangle.xMin /= sprite_texture.width;
                sprite_rectangle.xMax /= sprite_texture.width;
                sprite_rectangle.yMin /= sprite_texture.height;
                sprite_rectangle.yMax /= sprite_texture.height;
                GUI.DrawTextureWithTexCoords(rect, sprite_texture, sprite_rectangle);
            }
        }
    }

    public TechnologyCache GetCache()
    {
        return m_other_window_cache;
    }

    public void SetOtherWindowCache(TechnologyCache a_cache)
    {
        m_other_window_cache = a_cache;
    }
    private bool CheckOtherWindowContainer()
    {
        return m_other_window_cache != null
            && m_other_window_cache.m_cache != null
            && m_other_window_cache.m_cache.Count > 0;
    }
}

