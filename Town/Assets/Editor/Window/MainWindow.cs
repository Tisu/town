﻿
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;

class MainWindow : EditorWindow
{
    const float PIXELS_SPACE = 10f;
    public const float MAX_WINDOW_WIDTH = 800F;
    public const float MAX_WINDOW_HEIGTH = 200F;
    public const float MAX_SCROLLBAR_WIDTH = 200F;
    Vector2 m_scroll_position = Vector2.zero;
    IWindowDisplay m_current_window_display;
    IDictionary<string, IWindowDisplay> m_display_dictionary;

    [MenuItem("Tisu/MainWindow")]
    public static void Init()
    {
        var window = GetWindow<MainWindow>();
        window.minSize = new Vector2(MAX_WINDOW_WIDTH, MAX_WINDOW_HEIGTH);
        window.Show();
    }
    public void OnEnable()
    {
        InitializeDisplays();
        m_current_window_display = m_display_dictionary.First().Value;
    }

    private void InitializeDisplays()
    {
        var factory = new DisplaysDictionaryFactory();
        m_display_dictionary = factory.GetDisplays();
        foreach (var display in m_display_dictionary.Values)
        {
            display.OnEnable();
        }
        factory.BuildDependencies();       
    }

    public void OnDestroy()
    {
        foreach (var display in m_display_dictionary.Values)
        {
            display.OnDestroy();
        }       
    }
    public void OnGUI()
    {
        VerticalDisplay(TopButtons);
        HorizontalDisplay(ListView, Properties);
    }

    private void ListView()
    {
        m_scroll_position = GUILayout.BeginScrollView(m_scroll_position,
            "Box",
            GUILayout.ExpandHeight(true),
            GUILayout.Width(MAX_SCROLLBAR_WIDTH));
        m_current_window_display.DisplayListView();
        GUILayout.EndScrollView();
    }
    private void TopButtons()
    {
        HorizontalDisplay(CategoryButtons);
        GUILayout.Space(PIXELS_SPACE);
        HorizontalDisplay(ActiveButtons);
    }

    private void CategoryButtons()
    {
        foreach (var display_dictionary in m_display_dictionary)
        {
            if (display_dictionary.Value == m_current_window_display)
            {
                GUI.backgroundColor = Color.gray;
            }
            else
            {
                GUI.backgroundColor = Color.white;
            }
            if (GUILayout.Button(display_dictionary.Key))
            {
                ChangeFocus();               
                m_current_window_display = display_dictionary.Value;               
            }
        }
        GUI.backgroundColor = Color.white;
    }

    private void ActiveButtons()
    {
        if (GUILayout.Button("Add new " + m_current_window_display.ButtonEndName()))
        {
            m_current_window_display.AddNewToCache();
        }
        if (GUILayout.Button("Delete current " + m_current_window_display.ButtonEndName()))
        {
            m_current_window_display.DeleteCurrent();
        }
    }

    private void Properties()
    {
        EditorGUILayout.BeginVertical("Box", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        m_current_window_display.DisplayProperties();
        EditorGUILayout.EndVertical();
    }

    private void HorizontalDisplay(System.Action a_action)
    {
        GUILayout.BeginHorizontal();
        a_action();
        GUILayout.EndHorizontal();
    }
    private void HorizontalDisplay(System.Action a_action_one, System.Action a_action_two)
    {
        GUILayout.BeginHorizontal();
        a_action_one();
        a_action_two();
        GUILayout.EndHorizontal();
    }
    private void VerticalDisplay(System.Action a_action)
    {
        GUILayout.BeginVertical();
        a_action();
        GUILayout.EndVertical();
    }
   
    private void ChangeFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }
}

