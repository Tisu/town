﻿
using System.Collections.Generic;
using UnityEngine;

class InsertIntoBuildingDictionary
{
    readonly string FILE_PATH = Application.dataPath + @"/Scripts/Buildings/Semi_automatically_generated/BuildingResourceDictionary.cs";
    readonly string START_CONTENT = "static BuildingResourceDictionary()\r\n    {";
    public InsertIntoBuildingDictionary()
    {



    }
    public void Insert(IEnumerable<BuildingInformationAsset> a_buildings_cache)
    {
        var insertor = new ExistingClassInsertor(FILE_PATH, START_CONTENT);
        foreach (var building in a_buildings_cache)
        {
            insertor.InsertNewLine(BuildStringToInsert(building));
        }
        insertor.Dispose();
    }
    private string BuildStringToInsert(BuildingInformationAsset a_bulding)
    {
        if (a_bulding.GetSprite() == null)
        {
            Tisu.Logger.LogError("Sprite name is null! ");
            return null;
        }
        return "Register(\"" + a_bulding.GetSpriteName() + "\", new BuildingResources( " + a_bulding.GetStoneCost() + "," + a_bulding.GetWoodCost() + "));";
    }
}

