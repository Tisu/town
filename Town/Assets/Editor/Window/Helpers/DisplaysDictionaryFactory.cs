﻿
using System.Collections.Generic;
using System.Linq;

class DisplaysDictionaryFactory
{
    public DisplaysDictionaryFactory()
    {
    }
    IDictionary<string, IWindowDisplay> m_display_dictionary = new Dictionary<string, IWindowDisplay>()
    {
        { "Buildings",new BuildingDisplay() },
        { "Technology", new TechnologyDisplay() }
    };

    public IDictionary<string, IWindowDisplay> GetDisplays()
    {
        return m_display_dictionary;
    }
    public void BuildDependencies()
    {
        IWindowDisplay buildings;
        m_display_dictionary.TryGetValue("Buildings", out buildings);

        IWindowDisplay technology;
        m_display_dictionary.TryGetValue("Technology", out technology);

        TechnologyDisplay technology_display = technology as TechnologyDisplay;
        BuildingDisplay building_display = buildings as BuildingDisplay;

        if (technology_display != null && building_display != null)
        {
            building_display.SetOtherWindowCache(technology_display.GetCache());
        }
    }
}
