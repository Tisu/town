﻿
internal interface IWindowDisplay
{
    void DisplayListView();
    void DisplayProperties();
    void AddNewToCache();
    void DeleteCurrent();
    void OnEnable();
    void OnDestroy();
    string ButtonEndName();   
}

